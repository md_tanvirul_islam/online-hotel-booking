<?php

use App\Hotel;
use App\LocationDistrict;
use App\LocationDivision;
use App\Role;
use App\User;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','CustomerController@index')->name('customer_index');
Route::get('/destinations','CustomerController@destinations')->name('customer_destinations');
Route::get('/about','CustomerController@about')->name('customer_about');
Route::get('/contract','CustomerController@contract')->name('customer_contract');
Route::get('/elements','CustomerController@elements')->name('customer_elements');
Route::get('/news','CustomerController@news')->name('customer_news');
Route::match(['get','post'],'/search','SearchController@search')->name('customer.search');
Route::match(['get','post'],'/search/show','SearchController@show')->name('customer.search.show');
Route::match(['get','post'],'/booking','CustomerBookingController@bookingPage')->name('customer.booking.index');
Route::match(['get','post'],'/booking/store','CustomerBookingController@bookingStore')->name('customer.booking.store');
Route::get('/profile/{id}','ProfileController@index')->name('profile.index');
Route::post('/profile/create','ProfileController@store')->name('profile.store');
Route::get('/profile/{id}/edit','ProfileController@edit')->name('profile.edit');

//Route::match(['get', 'post'], '/', function () {
////
//});

// manager Routes
//, 'middleware' => []

Route::get('manager/register','ManagerRegisterController@showRegistrationForm')->name('ManagerRegister');
Route::post('manager/register','ManagerRegisterController@register')->name('ManagerRegister');
Route::group(['prefix' => 'manager','middleware'=>['auth']], function()
{
    Route::get('/', 'ManagerDashboardController@index')->name('manager.dashboard.index');
    Route::resource('hotels', HotelController::class);
    Route::resource('rooms', RoomController::class);
    Route::get('/bookings/pay/{id}','BookingController@pay')->name('bookings.pay');
    Route::resource('bookings', BookingController::class);
    Route::get('/users','HotelCustomerController@index')->name('manager.customer.index');



});



// admin Routes
//, 'middleware' => []
Route::group(['prefix' => 'admin','middleware'=>['auth']], function()
{
    Route::get('/', 'AdminDashboardController@index')->name('admin.dashboard.index');
    Route::resource('divisions', 'LocationDivisionController');
    Route::resource('districts', 'LocationDistrictController');
    Route::resource('roles',RoleController::class);

    Route::get('/users/admins','UserController@admin')->name('users.admin');
    Route::get('/users/customers','UserController@customer')->name('users.customer');
    Route::get('/users/managers','UserController@manager')->name('users.manager');
    Route::resource('users',UserController::class );

    Route::resource('hotel_lists', AdminHotelController::class);
//Hotel to Room Routes
    Route::get('/hotel_lists/{id}/bookings/index/','AdminHoteltoRoomController@hotelBookings')->name('Admin.hotel.booking.index');
    Route::get('/hotel_lists/{id}/rooms/index/','AdminHoteltoRoomController@index')->name('Admin.hotel.rooms.index');
    Route::get('/hotel_lists/{id}/rooms/create','AdminHoteltoRoomController@create')->name('Admin.hotel.rooms.create');
    Route::post('/hotel_lists/{id}/rooms','AdminHoteltoRoomController@store')->name('Admin.hotel.rooms.store');
    Route::get('/hotel_lists/{id}/rooms/{room}','AdminHoteltoRoomController@show')->name('Admin.hotel.rooms.show');
    Route::get('/hotel_lists/{id}/rooms/{room}/edit','AdminHoteltoRoomController@edit')->name('Admin.hotel.rooms.edit');
    Route::put('/hotel_lists/{id}/rooms/{room}','AdminHoteltoRoomController@update')->name('Admin.hotel.rooms.update');
    Route::delete('/hotel_lists/{id}/rooms/{room}','AdminHoteltoRoomController@destroy')->name('Admin.hotel.rooms.destroy');

    Route::resource('room_lists', AdminRoomController::class);
    Route::get('/bookings','AdminBookingListController@index')->name('admin.booking.index');
});






Route::get('def','UserController@customer')->name('def');
Route::get('ghi','UserController@manager')->name('ghi');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');







// laravel default routes
//Route::get('/', function () {
//    return view('welcome');
//});



// testing and demo routes

Route::get('/testing/blade', function (Request $request) {
  return view('testing');
})->name('testing.blade');

Route::get('/testing', function (Request $request) {
    return redirect()->route('admin.booking.index');
});

//    Route::resources([
//        'hotels' => HotelController::class,
//        'hotel_facilities' => HotelFacilityController::class,
//
//
//    ]);
