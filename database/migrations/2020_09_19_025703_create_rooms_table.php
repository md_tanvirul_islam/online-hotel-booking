<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->id();
            $table->foreignId('hotel_id')->constrained('hotels');
            $table->string('number',20);
            $table->string('type',30);
            $table->integer('single_beds');
            $table->integer('double_beds');
            $table->integer('max_person');
            $table->integer('price');
            $table->boolean('TV')->nullable()->default(true);
            $table->boolean('AC')->nullable()->default(true);
            $table->boolean('bathtub')->nullable()->default(false);
            $table->boolean('water_heater')->nullable()->default(true);
            $table->boolean('refrigerator')->nullable()->default(false);
            $table->boolean('wifi')->nullable()->default(true);
            $table->boolean('coffee_tea_maker')->nullable()->default(true);
            $table->text('other_facilities')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
