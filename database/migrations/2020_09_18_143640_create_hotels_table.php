<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotels', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users');
            $table->string('name');
            $table->foreignId('division_id')->constrained('location_divisions');
            $table->foreignId('district_id')->constrained('location_districts');
            $table->string('local_address',300);
            $table->string('telephone',20);
            $table->string('mobile1',20) ;
            $table->string('mobile2',20) ;
            $table->integer('star');
            $table->boolean('free_breakfast')->nullable()->default(true);
            $table->string('about_breakfast','200')->nullable()->default("details about breakfast");
            $table->boolean('private_parking')->nullable()->default(true);
            $table->boolean('car_rental')->nullable()->default(false);
            $table->boolean('swimming_pool')->nullable()->default(true);
            $table->boolean('call_on_doctor')->nullable()->default(false);
            $table->boolean('gym')->nullable()->default(true);
            $table->boolean('restaurant')->nullable()->default(false);
            $table->boolean('spa')->nullable()->default(true);
            $table->boolean('meeting_room')->nullable()->default(false);
            $table->text('other_facilities')->nullable();
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotels');
    }
}
