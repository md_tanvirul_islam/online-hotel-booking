<?php

use Illuminate\Database\Seeder;

class RoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rooms =array(
            array('hotel_id'=>1,'number'=>'Room1001','type'=>'Single Bed','single_beds'=>1,'double_beds'=>0,'max_person'=>1,'price'=>rand(1500,3000)),
            array('hotel_id'=>1,'number'=>'Room1002','type'=>'Single Bed','single_beds'=>1,'double_beds'=>1,'max_person'=>3,'price'=>rand(1500,3000)),
            array('hotel_id'=>2,'number'=>'Room1001','type'=>'Single Bed','single_beds'=>0,'double_beds'=>1,'max_person'=>2,'price'=>rand(1500,3000)),
            array('hotel_id'=>2,'number'=>'Room1002','type'=>'Single Bed','single_beds'=>0,'double_beds'=>2,'max_person'=>4,'price'=>rand(1500,3000)),
            array('hotel_id'=>3,'number'=>'Room1001','type'=>'Single Bed','single_beds'=>2,'double_beds'=>0,'max_person'=>2,'price'=>rand(1500,3000)),
            array('hotel_id'=>3,'number'=>'Room1002','type'=>'Single Bed','single_beds'=>2,'double_beds'=>1,'max_person'=>4,'price'=>rand(1500,3000)),
            array('hotel_id'=>4,'number'=>'Room1001','type'=>'Single Bed','single_beds'=>1,'double_beds'=>1,'max_person'=>3,'price'=>rand(1500,3000)),
            array('hotel_id'=>4,'number'=>'Room1002','type'=>'Single Bed','single_beds'=>1,'double_beds'=>0,'max_person'=>2,'price'=>rand(1500,3000)),
            array('hotel_id'=>5,'number'=>'Room1001','type'=>'Single Bed','single_beds'=>1,'double_beds'=>1,'max_person'=>3,'price'=>rand(1500,3000)),
            array('hotel_id'=>5,'number'=>'Room1002','type'=>'Single Bed','single_beds'=>0,'double_beds'=>1,'max_person'=>2,'price'=>rand(1500,3000)),
            array('hotel_id'=>6,'number'=>'Room1001','type'=>'Single Bed','single_beds'=>1,'double_beds'=>1,'max_person'=>2,'price'=>rand(1500,3000)),
            array('hotel_id'=>6,'number'=>'Room1002','type'=>'Single Bed','single_beds'=>0,'double_beds'=>1,'max_person'=>2,'price'=>rand(1500,3000)),
            array('hotel_id'=>7,'number'=>'Room1001','type'=>'Single Bed','single_beds'=>1,'double_beds'=>1,'max_person'=>3,'price'=>rand(1500,3000)),
            array('hotel_id'=>7,'number'=>'Room1002','type'=>'Single Bed','single_beds'=>0,'double_beds'=>2,'max_person'=>4,'price'=>rand(1500,3000)),
            array('hotel_id'=>8,'number'=>'Room1001','type'=>'Single Bed','single_beds'=>1,'double_beds'=>0,'max_person'=>1,'price'=>rand(1500,3000)),
            array('hotel_id'=>8,'number'=>'Room1002','type'=>'Single Bed','single_beds'=>1,'double_beds'=>2,'max_person'=>3,'price'=>rand(1500,3000)),
            array('hotel_id'=>9,'number'=>'Room1001','type'=>'Single Bed','single_beds'=>1,'double_beds'=>0,'max_person'=>1,'price'=>rand(1500,3000)),
            array('hotel_id'=>9,'number'=>'Room1002','type'=>'Single Bed','single_beds'=>2,'double_beds'=>0,'max_person'=>2,'price'=>rand(1500,3000)),
            array('hotel_id'=>10,'number'=>'Room1001','type'=>'Single Bed','single_beds'=>1,'double_beds'=>0,'max_person'=>1,'price'=>rand(1500,3000)),
            array('hotel_id'=>10,'number'=>'Room1002','type'=>'Single Bed','single_beds'=>1,'double_beds'=>1,'max_person'=>3,'price'=>rand(1500,3000)),
            array('hotel_id'=>11,'number'=>'Room1001','type'=>'Single Bed','single_beds'=>1,'double_beds'=>0,'max_person'=>1,'price'=>rand(1500,3000)),
            array('hotel_id'=>11,'number'=>'Room1003','type'=>'Single Bed','single_beds'=>1,'double_beds'=>0,'max_person'=>1,'price'=>rand(1500,3000)),
            array('hotel_id'=>12,'number'=>'Room1002','type'=>'Single Bed','single_beds'=>1,'double_beds'=>0,'max_person'=>1,'price'=>rand(1500,3000)),
            array('hotel_id'=>12,'number'=>'Room1001','type'=>'Single Bed','single_beds'=>1,'double_beds'=>0,'max_person'=>1,'price'=>rand(1500,3000)),
            array('hotel_id'=>13,'number'=>'Room1002','type'=>'Single Bed','single_beds'=>1,'double_beds'=>0,'max_person'=>1,'price'=>rand(1500,3000)),
            array('hotel_id'=>13,'number'=>'Room1001','type'=>'Single Bed','single_beds'=>1,'double_beds'=>0,'max_person'=>1,'price'=>rand(1500,3000)),
            array('hotel_id'=>14,'number'=>'Room1002','type'=>'Single Bed','single_beds'=>1,'double_beds'=>0,'max_person'=>1,'price'=>rand(1500,3000)),
            array('hotel_id'=>14,'number'=>'Room1001','type'=>'Single Bed','single_beds'=>1,'double_beds'=>0,'max_person'=>1,'price'=>rand(1500,3000)),
            array('hotel_id'=>15,'number'=>'Room1002','type'=>'Single Bed','single_beds'=>1,'double_beds'=>1,'max_person'=>2,'price'=>rand(1500,3000)),
            array('hotel_id'=>15,'number'=>'Room1001','type'=>'Single Bed','single_beds'=>1,'double_beds'=>1,'max_person'=>2,'price'=>rand(1500,3000)),
            array('hotel_id'=>16,'number'=>'Room1002','type'=>'Single Bed','single_beds'=>2,'double_beds'=>0,'max_person'=>2,'price'=>rand(1500,3000)),
            array('hotel_id'=>16,'number'=>'Room1001','type'=>'Single Bed','single_beds'=>3,'double_beds'=>0,'max_person'=>3,'price'=>rand(1500,3000)),

        );
        DB::table('rooms')->insert($rooms);
    }
}
