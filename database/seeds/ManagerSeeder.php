<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class ManagerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role =\App\Role::where('name','=','manager')->first();
        $managers = array(
            array('name'=>'manager 1','email'=>'manager1@mail.com','password'=>Hash::make('password'),'role_id'=>"$role->id"),
            array('name'=>'manager 2','email'=>'manager2@mail.com','password'=>Hash::make('password'),'role_id'=>"$role->id"),
            array('name'=>'manager 3','email'=>'manager3@mail.com','password'=>Hash::make('password'),'role_id'=>"$role->id"),
            array('name'=>'manager 4','email'=>'manager4@mail.com','password'=>Hash::make('password'),'role_id'=>"$role->id"),
            array('name'=>'manager 5','email'=>'manager5@mail.com','password'=>Hash::make('password'),'role_id'=>"$role->id"),
            array('name'=>'manager 6','email'=>'manager6@mail.com','password'=>Hash::make('password'),'role_id'=>"$role->id"),
            array('name'=>'manager 7','email'=>'manager7@mail.com','password'=>Hash::make('password'),'role_id'=>"$role->id"),
            array('name'=>'manager 8','email'=>'manager8@mail.com','password'=>Hash::make('password'),'role_id'=>"$role->id"),
            array('name'=>'manager 9','email'=>'manager9@mail.com','password'=>Hash::make('password'),'role_id'=>"$role->id"),
            array('name'=>'manager 10','email'=>'manager10@mail.com','password'=>Hash::make('password'),'role_id'=>"$role->id"),
            array('name'=>'manager 11','email'=>'manager11@mail.com','password'=>Hash::make('password'),'role_id'=>"$role->id"),
            array('name'=>'manager 12','email'=>'manager12@mail.com','password'=>Hash::make('password'),'role_id'=>"$role->id"),
            array('name'=>'manager 13','email'=>'manager13@mail.com','password'=>Hash::make('password'),'role_id'=>"$role->id"),
            array('name'=>'manager 14','email'=>'manager14@mail.com','password'=>Hash::make('password'),'role_id'=>"$role->id"),
            array('name'=>'manager 15','email'=>'manager15@mail.com','password'=>Hash::make('password'),'role_id'=>"$role->id"),
            array('name'=>'manager 16','email'=>'manager16@mail.com','password'=>Hash::make('password'),'role_id'=>"$role->id"),
            array('name'=>'manager 17','email'=>'manager17@mail.com','password'=>Hash::make('password'),'role_id'=>"$role->id"),
            array('name'=>'manager 18','email'=>'manager18@mail.com','password'=>Hash::make('password'),'role_id'=>"$role->id"),
            array('name'=>'manager 19','email'=>'manager19@mail.com','password'=>Hash::make('password'),'role_id'=>"$role->id"),
            array('name'=>'manager 20','email'=>'manager20@mail.com','password'=>Hash::make('password'),'role_id'=>"$role->id"),


        );
        DB::table('users')->insert($managers);
    }

}
