<?php

use Illuminate\Database\Seeder;

class HotelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hotels = array(
            array('user_id'=>'1','name'=>'hotel 1','division_id'=>1,'district_id'=>1,'local_address'=>'hotel 1 address','telephone'=>'2216590','mobile1'=>'01717377000','mobile2'=>'01919377000','star'=>'4'),
            array('user_id'=>'2','name'=>'hotel 2','division_id'=>1,'district_id'=>8,'local_address'=>'hotel 2 address','telephone'=>'2216590','mobile1'=>'01717377000','mobile2'=>'01919377000','star'=>'4'),
            array('user_id'=>'3','name'=>'hotel 3','division_id'=>2,'district_id'=>14,'local_address'=>'hotel 3 address','telephone'=>'2216590','mobile1'=>'01717377000','mobile2'=>'01919377000','star'=>'4'),
            array('user_id'=>'4','name'=>'hotel 4','division_id'=>2,'district_id'=>15,'local_address'=>'hotel 4 address','telephone'=>'2216590','mobile1'=>'01717377000','mobile2'=>'01919377000','star'=>'4'),
            array('user_id'=>'5','name'=>'hotel 5','division_id'=>3,'district_id'=>20,'local_address'=>'hotel 5 address','telephone'=>'2216590','mobile1'=>'01717377000','mobile2'=>'01919377000','star'=>'4'),
            array('user_id'=>'6','name'=>'hotel 6','division_id'=>3,'district_id'=>27,'local_address'=>'hotel 6 address','telephone'=>'2216590','mobile1'=>'01717377000','mobile2'=>'01919377000','star'=>'4'),
            array('user_id'=>'7','name'=>'hotel 7','division_id'=>4,'district_id'=>33,'local_address'=>'hotel 7 address','telephone'=>'2216590','mobile1'=>'01717377000','mobile2'=>'01919377000','star'=>'4'),
            array('user_id'=>'8','name'=>'hotel 8','division_id'=>4,'district_id'=>34,'local_address'=>'hotel 8 address','telephone'=>'2216590','mobile1'=>'01717377000','mobile2'=>'01919377000','star'=>'4'),
            array('user_id'=>'9','name'=>'hotel 9','division_id'=>5,'district_id'=>36,'local_address'=>'hotel 9 address','telephone'=>'2216590','mobile1'=>'01717377000','mobile2'=>'01919377000','star'=>'4'),
            array('user_id'=>'10','name'=>'hotel 10','division_id'=>5,'district_id'=>38,'local_address'=>'hotel 10 address','telephone'=>'2216590','mobile1'=>'01717377000','mobile2'=>'01919377000','star'=>'4'),
            array('user_id'=>'11','name'=>'hotel 11','division_id'=>6,'district_id'=>41,'local_address'=>'hotel 11 address','telephone'=>'2216590','mobile1'=>'01717377000','mobile2'=>'01919377000','star'=>'4'),
            array('user_id'=>'12','name'=>'hotel 12','division_id'=>6,'district_id'=>47,'local_address'=>'hotel 12 address','telephone'=>'2216590','mobile1'=>'01717377000','mobile2'=>'01919377000','star'=>'4'),
            array('user_id'=>'13','name'=>'hotel 13','division_id'=>7,'district_id'=>54,'local_address'=>'hotel 13 address','telephone'=>'2216590','mobile1'=>'01717377000','mobile2'=>'01919377000','star'=>'4'),
            array('user_id'=>'14','name'=>'hotel 14','division_id'=>7,'district_id'=>59,'local_address'=>'hotel 14 address','telephone'=>'2216590','mobile1'=>'01717377000','mobile2'=>'01919377000','star'=>'4'),
            array('user_id'=>'15','name'=>'hotel 15','division_id'=>8,'district_id'=>62,'local_address'=>'hotel 15 address','telephone'=>'2216590','mobile1'=>'01717377000','mobile2'=>'01919377000','star'=>'4'),
            array('user_id'=>'16','name'=>'hotel 16','division_id'=>8,'district_id'=>64,'local_address'=>'hotel 16 address','telephone'=>'2216590','mobile1'=>'01717377000','mobile2'=>'01919377000','star'=>'4'),
        );
        DB::table('hotels')->insert($hotels);
    }
}
