<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = array(
            array('id'=>'1','name'=>'admin'),
            array('id'=>'2','name'=>'manager'),
            array('id'=>'3','name'=>'customer'),
        );
        DB::table('roles')->insert($roles);
    }
}
