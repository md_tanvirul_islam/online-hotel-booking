<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role =\App\Role::where('name','=','customer')->first();
        $users = array(
            array('name'=>'Customer 1','email'=>'customer1@mail.com','password'=>Hash::make('password'),'role_id'=>"$role->id"),
            array('name'=>'Customer 2','email'=>'customer2@mail.com','password'=>Hash::make('password'),'role_id'=>"$role->id"),
            array('name'=>'Customer 3','email'=>'customer3@mail.com','password'=>Hash::make('password'),'role_id'=>"$role->id"),
            array('name'=>'Customer 4','email'=>'customer4@mail.com','password'=>Hash::make('password'),'role_id'=>"$role->id"),
            array('name'=>'Customer 6','email'=>'customer6@mail.com','password'=>Hash::make('password'),'role_id'=>"$role->id"),
            array('name'=>'Customer 5','email'=>'customer5@mail.com','password'=>Hash::make('password'),'role_id'=>"$role->id"),

        );
        DB::table('users')->insert($users);
    }
}
