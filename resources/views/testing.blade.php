@include('frontend.message')
@include('backend.layouts.errors')

{{--@foreach ($hello as $mno)--}}

{{-- {{$mno['price']}}--}}
{{--    <br>--}}

{{--@endforeach--}}


{{--foreach loop in blade--}}

{{--            <ul>--}}
{{--                @foreach($divisions->all() as $division)--}}
{{--                    <li>{{$division}}</li>--}}
{{--                @endforeach--}}
{{--            </ul>--}}

{{--            <ul>--}}
{{--                @foreach($districts->all() as $division)--}}
{{--                    <li>{{$division}}</li>--}}
{{--                @endforeach--}}
{{--            </ul>--}}


{{--form start and close--}}

{{--{!! Form::open(['route'=>'hotels.store']) !!}--}}

{{--@include('backend.manager.hotel_profile.form')--}}

{{--{!! Form::close() !!}--}}

{{--form text number textarea input--}}
{{--<div class="form-group col-4">--}}
{{--    {!! Form::label('mobile1','Another Mobile No:') !!}--}}
{{--    {!! Form::text('mobile1',null,['class'=>'form-control','required']) !!}--}}
{{--</div>--}}


{{--form radio and check in button--}}

{{--<div class="form-group col-2 text-center" >--}}
{{--    <h5>Rooms for Meeting</h5>--}}
{{--    {!! Form::label('meeting_room','yes') !!}--}}
{{--    {!! Form::radio('meeting_room', '1') !!}<br>--}}
{{--    {!! Form::label('meeting_room','No') !!}--}}
{{--    {!! Form::radio('meeting_room', '0') !!}--}}
{{--</div>--}}


{{--submit / update button--}}

{{--<div class="form-row">--}}
{{--    <div class="col-4 text-center"></div>--}}
{{--    <div class="col-4 text-center">--}}
{{--        {!! Form::submit('Register Your Hotel',['class'=>['btn','btn-primary'] ]) !!}--}}
{{--    </div>--}}
{{--    <div class="col-4 text-center"></div>--}}
{{--</div>--}}




@php
    $rooms = session('rooms');
    $divisions = session('divisions');
    $districts = session('districts');
    $checkin = session('checkin');
    $checkout = session('checkout');
@endphp

@foreach($rooms as $room)
    <!-- Destination -->
    <div class="destination item">
        <div class="destination_image">
            <img src="{{asset('ui/frontend/images/destination_1.jpg')}}" style="width: 350px !important;height: 260px !important;" alt="">
        </div>

        @php
            $hotel= \App\Hotel::where('id','=',"$room->hotel_id")->first();
        @endphp

        <div class="destination_content">
            <div class="destination_title"><a href="#">{{$room->type}}</a></div>
            <div class="destination_subtitle"><p>{{$hotel->name}}</p></div>
            <div class="destination_price">Price ${{$room->price}}</div>
            <div class="destination_list">
                <ul>
                    <li>{{$hotel->star}} Stars Hotel</li>
                    <li>Maximum Person :{{$room->max_person}}</li>
                    <li>Free Breakfast: {{$hotel->free_breakfast?"Yes":"No"}}</li>
                    <li>AC: {{$room->AC?"Yes":"No"}}</li>
                    <form style="display: inline" action="{{route('customer.search.show')}}" method="post" target="_blank" >
                        @csrf
                        <input type="text" value="{{$room->id}}" name="room_id" hidden>
                        <input type="text" value="{{$hotel->id}}"  name="hotel_id" hidden>
                        <input type="date" value="{{$checkin}}" name="checkin" hidden>
                        <input type="date" value="{{$checkout}}" name="checkout" hidden>
                        <input class="btn btn-warning text-dark" type="submit" value="Show Details">

                    </form>
                    <form style="display: inline" action="{{route('customer.booking.index')}}" method="post" target="_blank" >
                        @csrf
                        <input type="text" value="{{$room->id}}" name="room_id" hidden>
                        <input type="text" value="{{$hotel->id}}"  name="hotel_id" hidden>
                        <input type="date" value="{{$checkin}}" name="checkin" hidden>
                        <input type="date" value="{{$checkout}}" name="checkout" hidden>
                        <input class="btn btn-success text-light" type="submit" value="Book This Room">

                    </form>

                </ul>
            </div>
        </div>
    </div>
@endforeach
