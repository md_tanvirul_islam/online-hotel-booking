@extends('frontend.master')

@push('css')
    <link rel="stylesheet" type="text/css" href="{{asset('ui/frontend/styles/search_show.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('ui/frontend/styles/destinations_responsive.css')}}">
@endpush

@section('content')

    <!-- Home -->

    <div class="home">
        <div class="background_image" style="background-image:url({{asset('ui/frontend/images/destinations.jpg')}})"></div>
    </div>
    <div class="container-fluid">
        <div class="row" style="text-align: center">
            <div class="col text-dark"><h3> {{$hotel->name}} | {{$hotel->star}} stars Hotel</h3></div>
        </div>
        <div class="row">
            <div style="text-align: left;" class="col text-dark"><h4>Hotel Details</h4></div>
            <div class="col text-light" style="text-align: right">
                <form action="{{route('customer.booking.index')}}" method="post" >
                    @csrf
                    <input type="text" value="{{$room->id}}" name="room_id" hidden>
                    <input type="text" value="{{$hotel->id}}"  name="hotel_id" hidden>
                    <input type="date" value="{{$checkin}}" name="checkin" hidden>
                    <input type="date" value="{{$checkout}}" name="checkout" hidden>
                    <button class="btn btn-success text-light" type="submit" > Book This Room </button>

                </form>
            </div>
        </div>
        <hr>

        <div class="row">
            <div class="col" style="width: 50%;height: 540px">
                @if(strpos("$hotel->photo","images.unsplash.com/"))
                    <img src="{{$hotel->photo}}" style="width: 100%;height:540px" alt="">
                @else
                    <img src='{{asset("uploads/hotels/$hotel->photo")}}' style="width: 100%;height:540px" alt="">
                @endif
            </div>
            <div class="col" style="width: 50%;height: 540px">
                <div class="table">
                    {{--            location--}}
                    <h5 class="text-info">Address:</h5>
                    <div class="row" style="text-align: center">
                        <div class="col text-dark"><strong>Division</strong></div>
                        <div class="col text-dark"><strong>District</strong></div>
                        <div class="col text-dark"><strong>Local Address</strong></div>
                    </div>
                    <div class="row" style="text-align: center">

                        <div class="col text-dark">{{$division->name}}</div>
                        <div class="col text-dark">{{$district->name}}</div>
                        <div class="col text-dark">{{$hotel->local_address}}</div>
                    </div>
                    {{--            contract numbers--}}
                    <h5 class="text-info">Contract:</h5>
                    <div class="row" style="text-align: center">
                        <div class="col text-dark"><strong>Mobile1</strong></div>
                        <div class="col text-dark"><strong>Mobile2</strong></div>
                        <div class="col text-dark"><strong>Telephone</strong></div>
                    </div>
                    <div class="row" style="text-align: center">

                        <div class="col text-dark">{{$hotel->mobile1}}</div>
                        <div class="col text-dark">{{$hotel->mobile2}}</div>
                        <div class="col text-dark">{{$hotel->telephone}}</div>
                    </div>
                    {{--            hotel facilities--}}
                    <h5 class="text-info">Facilities:</h5>
                    <div class="row" style="text-align: center">
                        <div class="col text-dark"><strong>Free Breakfast</strong></div>
                        <div class="col text-dark"><strong>Private Parking</strong></div>
                        <div class="col text-dark"><strong>Car Rental</strong></div>
                        <div class="col text-dark"><strong>Swimming Pool</strong></div>
                    </div>
                    <div class="row" style="text-align: center">

                        <div class="col text-dark">{{$hotel->free_breakfast?"Yes":"No"}}</div>
                        <div class="col text-dark">{{$hotel->private_parking?"Yes":"No"}}</div>
                        <div class="col text-dark">{{$hotel->car_rental?"Yes":"No"}}</div>
                        <div class="col text-dark">{{$hotel->swimming_pool?"Yes":"No"}}</div>
                    </div>

                    <div class="row" style="margin-top:10px;text-align: center">
                        <div style="width: 25%" class=" text-dark"><strong>Call on Doctor</strong></div>
                        <div style="width: 25%" class=" text-dark"><strong>Fitness Gym</strong></div>
                        <div style="width: 25%" class=" text-dark"><strong>Restaurant</strong></div>
                        <div style="width: 25%" class=" text-dark"><strong>Spa </strong></div>
                    </div>


                    <div class="row" style="text-align: center">

                        <div style="width: 25%" class=" text-dark">{{$hotel->call_on_doctor?"Yes":"No"}}</div>
                        <div style="width: 25%" class=" text-dark">{{$hotel->gym?"Yes":"No"}}</div>
                        <div style="width: 25%" class=" text-dark">{{$hotel->restaurant?"Yes":"No"}}</div>
                        <div style="width: 25%" class=" text-dark">{{$hotel->spa?"Yes":"No"}}</div>
                    </div>
                    <div style="margin-top: 20px" class="row text-dark">
                        <div class="col"><strong>About Free Breakfast</strong></div>
                        <div class="col"><strong>Other Facilities</strong></div>
                    </div>
                    <div  class="row">
                        <div class="col">
                            <p>{{$hotel->about_breakfast}}</p>
                        </div>
                        <div class="col">
                            <p>{{$hotel->other_facilities}}</p>
                        </div>
                    </div>

                </div>
            </div>

        </div>

{{--        room details--}}
        <div class="row" style="margin-top: 20px">
            <div style="text-align: left;" class="col text-dark"><h4>Room Details</h4></div>
        </div>
        <hr>
        <div style="margin-bottom: 20px" class="row">
            <div style="margin-left: 5px">
                <div class="row">
                    <div class="col" style="width: 50%;height: 390px">

                        {{--            location--}}
                        <h5 class="text-info">Info</h5>
                        <div class="row" style="text-align: center">
                            <div class="col text-dark"><strong>Room No.</strong></div>
                            <div class="col text-dark"><strong>Type</strong></div>
                            <div class="col text-dark"><strong>Price Per Day</strong></div>
                        </div>
                        <div class="row" style="text-align: center">
                            <div class="col text-dark">{{$room->number}}</div>
                            <div class="col text-dark">{{$room->type}} Room</div>
                            <div class="col text-dark">{{$room->price}} Taka</div>
                        </div>


                        {{--            contract numbers--}}
                        <h5 class="text-info">Beds and persons</h5>
                        <div class="row" style="text-align: center">
                            <div class="col text-dark"><strong>Single Beds</strong></div>
                            <div class="col text-dark"><strong>Double Beds</strong></div>
                            <div class="col text-dark"><strong>Maximum Person</strong></div>
                        </div>
                        <div class="row" style="text-align: center">

                            <div class="col text-dark">{{$room->single_beds}}</div>
                            <div class="col text-dark">{{$room->double_beds}}</div>
                            <div class="col text-dark">{{$room->max_person}}</div>
                        </div>


                        {{--            Room facilities--}}
                        <h5 class="text-info">Facilities:</h5>
                        <div class="row" style="text-align: center">
                            <div class="col text-dark"><strong>Ac</strong></div>
                            <div class="col text-dark"><strong>TV</strong></div>
                            <div class="col text-dark"><strong>Wifi</strong></div>
                        </div>
                        <div class="row" style="text-align: center">

                            <div class="col text-dark">{{$room->AC?"Yes":"No"}}</div>
                            <div class="col text-dark">{{$room->TV?"Yes":"No"}}</div>
                            <div class="col text-dark">{{$room->wifi?"Yes":"No"}}</div>
                        </div>

                        <div class="row" style="margin-top:10px;text-align: center">
                            <div  class="col text-dark"><strong>Refrigerator</strong></div>
                            <div  class="col text-dark"><strong>Coffee/Tea Maker</strong></div>
                            <div  class="col text-dark"><strong>Bathtub</strong></div>
                            <div  class="col text-dark"><strong>Water Heater</strong></div>
                        </div>
                        <div class="row" style="text-align: center">
                            <div  class="col text-dark">{{$room->refrigerator?"Yes":"No"}}</div>
                            <div  class="col text-dark">{{$room->coffee_tea_maker?"Yes":"No"}}</div>
                            <div  class="col text-dark">{{$room->bathtub?"Yes":"No"}}</div>
                            <div  class="col text-dark">{{$room->water_heater?"Yes":"No"}}</div>
                        </div>

                        <div style="margin-top: 20px" class="row text-dark">
                            <div class="col"><strong>About Other Facilities</strong></div>
                        </div>
                        <div  class="row">
                            <div class="col">
                                <p>{{$room->other_facilities}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col" style="width: 50%;height: 390px">
                        @if(strpos("$room->photo","images.unsplash.com/"))
                            <img src="{{$room->photo}}" style="width: 100%;height:390px" alt="">
                        @else
                            <img src='{{asset("uploads/rooms/$room->photo")}}' style="width: 100%;height:390px" alt="">
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="row" style="margin-bottom: 20px;margin-top: 15px;">
            <div class="col-md"></div>
            <div class="col-md">
                <form action="{{route('customer.booking.index')}}" method="post" >
                    @csrf
                    <input type="text" value="{{$room->id}}" name="room_id" hidden>
                    <input type="text" value="{{$hotel->id}}"  name="hotel_id" hidden>
                    <input type="date" value="{{$checkin}}" name="checkin" hidden>
                    <input type="date" value="{{$checkout}}" name="checkout" hidden>
                    <input class="btn btn-success btn-block text-light" type="submit" value="Book This Room">

                </form>
            </div>
            <div class="col-md"></div>

        </div>
    </div>




@endsection

@push('js')

    <script src="{{asset('ui/frontend/plugins/Isotope/isotope.pkgd.min.js')}}"></script>

    <script src="{{asset('ui/frontend/js/destinations.js')}}"></script>
@endpush
