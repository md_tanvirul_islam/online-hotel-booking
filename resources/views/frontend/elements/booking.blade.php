@extends('frontend.master')

@push('css')
    <link rel="stylesheet" type="text/css" href="{{asset('ui/frontend/styles/search_show.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('ui/frontend/styles/destinations_responsive.css')}}">
@endpush

@section('content')

    <!-- Home -->

    <div class="home">
        <div class="background_image" style="background-image:url({{asset('ui/frontend/images/destinations.jpg')}})"></div>
    </div>
    <div class="container-fluid" style="margin-bottom: 200px">
        <div class="row" style="text-align: center">
            <div class="col text-dark"><h3> {{auth()->user()->name}}</h3></div>
        </div>
        <div class="row">
            <div style="text-align: left;" class="col text-dark"><h4>Booking Details</h4></div>
        </div>
        <hr>
        <div class="table table-bordered" >
            <div class="row text-center text-dark font-weight-bold" style="padding-right: 5px">
                <div class="col">No.</div>
                <div class="col">Hotel Name</div>
                <div class="col">Room No.</div>
                <div class="col">Room Type</div>
                <div class="col">Beds</div>
                <div class="col">Max Person</div>
                <div class="col">CheckIn</div>
                <div class="col">CheckIn</div>
                <div class="col">Price P.D.[Taka]</div>
                <div class="col">Days</div>
                <div class="col">Amount[Taka]</div>
            </div>
            @php
                $i = 0;
            @endphp
                 <div class="row text-center text-dark">
                <div class="col">{{++$i}}</div>
                <div class="col">{{$hotel->name}}</div>
                <div class="col">{{$room->number}}</div>
                <div class="col">{{$room->type}}</div>
                     <div class="col">Single: {{$room->single_beds}} Double: {{$room->double_beds}}</div>
                <div class="col">{{$room->max_person}}</div>
                <div class="col">{{$checkin}}</div>
                <div class="col">{{$checkout}}</div>
                <div class="col">{{$room->price}}</div>
                <div class="col">{{$noOfDays}}</div>
                <div class="col">{{$amount}}</div>
            </div>
            <hr>
            <div class="row text-dark font-weight-bold">
                <div class="col-11 text-right">Total Amount</div>
                <div class=" col text-center">{{$amount}}</div>
            </div>

        </div>
        <div class="row text-light font-weight-bold" >
            <div class="col-5"></div>
            <div class="col-2" >
                @php
                   /* $booking['user_id']=auth()->id();
                    $booking['room_id']=$room->id;
                    $booking['checkin']=$checkin;
                    $booking['checkout']=$checkout;
                    $booking['noOfDays']=$noOfDays;
                    $booking['amount']=$amount;
                    //$booking_data =serialize($booking);
                   // $booking_data = json_encode($booking);
                    //dd($booking_data);*/
                @endphp
{{--                <a class="btn btn-success btn-block" href="{{route('customer.booking.store',[$booking_data])}}">Confirm Your Booking</a>--}}
                <form method="post" action="{{route('customer.booking.store')}}">
                    @csrf
                    <input type="text" name="user_id" hidden value="{{auth()->id()}}">
                    <input type="text" name="hotel_id" hidden value="{{$hotel->id}}">
                    <input type="text" name="room_id" hidden value="{{$room->id}}">
                    <input type="text" name="checkin" hidden value="{{$checkin}}">
                    <input type="text" name="checkout" hidden value="{{$checkout}}">
                    <input type="number" name="days" hidden value="{{$noOfDays}}">
                    <input type="number" name="cost" hidden value="{{$amount}}">
                    <input type="submit" class="btn btn-success btn-block"  style="width: 200px " value="Confirm Your Booking">
                </form>
            </div>
            <div class="col-5"></div>
        </div>

    </div>




@endsection

@push('js')

    <script src="{{asset('ui/frontend/plugins/Isotope/isotope.pkgd.min.js')}}"></script>

    <script src="{{asset('ui/frontend/js/destinations.js')}}"></script>
@endpush
