@extends('frontend.master')
@push('css')
    <link rel="stylesheet" type="text/css" href="{{asset('ui/frontend/styles/search_show.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('ui/frontend/styles/destinations_responsive.css')}}">
@endpush

@section('content')
    @php
        $rooms = session('rooms');
        $checkin = session('checkin');
        $checkout = session('checkout');
        $divisions = \App\LocationDivision::all();
        $districts = \App\LocationDistrict::all();
    @endphp
    <div class="home">
        <div class="background_image" style="background-image:url({{asset('ui/frontend/images/destinations.jpg')}})"></div>
    </div>

    <!-- Search -->

{{--    <div class="home_search ">--}}
{{--        <div class="container">--}}
{{--            <div class="row">--}}
{{--                <div class="col">--}}
{{--                    <div class="home_search_container bg-dark">--}}
{{--                        <div class="home_search_title">Search for your Hotel</div>--}}
{{--                        <div class="home_search_content">--}}
{{--                            <form action="{{route('customer.search')}}" method="post" class=" form-inline home_search_form" id="home_search_form">--}}
{{--                                <div class="d-flex flex-lg-row flex-column align-items-start justify-content-lg-between justify-content-start">--}}
{{--                                    @csrf--}}
{{--                                    <select name="division_id" class="search_input search_input_1" >--}}
{{--                                        <option>Divisions</option>--}}
{{--                                        @foreach($divisions as $division)--}}
{{--                                            <option value="{{$division->id}}">{{$division->name}} </option>--}}
{{--                                        @endforeach--}}
{{--                                    </select>--}}

{{--                                    <select name="district_id" class="search_input search_input_2" >--}}
{{--                                        <option>Districts</option>--}}
{{--                                        @foreach($districts as $district)--}}
{{--                                            <option value="{{$district->id}}">{{$district->name}} </option>--}}
{{--                                        @endforeach--}}
{{--                                    </select>--}}

{{--                                    <div class="form-group" style="margin-left: 5px!important; ">--}}
{{--                                        <label class="text-dark" for="checkin"><b>Check In&nbsp;&nbsp;&nbsp;</b> </label>--}}
{{--                                        <input type="date" name="checkin" id="checkin" class=" form-control search_input search_input_3" placeholder="Check In Date" required="required">--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group" style="margin-left: 5px!important; ">--}}
{{--                                        <label class="text-dark" for="checkout"><b>Check Out&nbsp;&nbsp;&nbsp;</b></label>--}}
{{--                                        <input type="date" name="checkout" id="checkout" class="form-control search_input search_input_4" placeholder="Check Out Date" required="required">--}}
{{--                                    </div>--}}
{{--                                    <input type="text" name="person" class="search_input search_input_4" placeholder="Person" required="required">--}}

{{--                                    <button class="home_search_button">search</button>--}}
{{--                                </div>--}}
{{--                            </form>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

    <!-- Destinations -->

    <div class="destinations" id="destinations">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <div class="section_subtitle">simply amazing places</div>
                    <div class="section_title"><h2>Matched Hotels' Rooms</h2></div>
                </div>
            </div>
            <div class="row destination_sorting_row">
                <div class="col">
                    <div class="destination_sorting d-flex flex-row align-items-start justify-content-start">
                        <div class="sorting">
                            <ul class="item_sorting">
                                <li>
                                    <span class="sorting_text">Sort By</span>
                                    <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                    <ul>
                                        <li class="product_sorting_btn" data-isotope-option='{ "sortBy": "original-order" }'><span>Show All</span></li>
                                        <li class="product_sorting_btn" data-isotope-option='{ "sortBy": "price" }'><span>Price</span></li>
                                        <li class="product_sorting_btn" data-isotope-option='{ "sortBy": "name" }'><span>Name</span></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="sort_box ml-auto"><i class="fa fa-th" aria-hidden="true"></i></div>
                    </div>
                </div>
            </div>
            <div class="row destinations_row">
                <div class="col">
                    <div class="destinations_container item_grid">

                    @foreach($rooms as $room)
                        <!-- Destination -->
                            <div class="destination item">
                                <div class="destination_image">
                                    @if(strpos("$room->photo","images.unsplash.com/"))
                                        <img src="{{$room->photo}}" style="width: 350px !important;height: 260px !important;" alt="">
                                    @else
                                        <img src='{{asset("uploads/rooms/$room->photo")}}' style="width: 350px !important;height: 260px !important;" alt="">
                                    @endif
                                </div>

                                @php
                                    $hotel= \App\Hotel::where('id','=',"$room->hotel_id")->first();
                                @endphp

                                <div class="destination_content">
                                    <div class="destination_title"><a href="#">{{$room->type}}</a></div>
                                    <div class="destination_subtitle"><p>{{$hotel->name}}</p></div>
                                    <div class="destination_price">Price BDT{{$room->price}}</div>
                                    <div class="destination_list">
                                        <ul>
                                            <li>{{$hotel->star}} Stars Hotel</li>
                                            <li>Maximum Person :{{$room->max_person}}</li>
                                            <li>Free Breakfast: {{$hotel->free_breakfast?"Yes":"No"}}</li>
                                            <li>AC: {{$room->AC?"Yes":"No"}}</li>
                                            <form style="display: inline" action="{{route('customer.search.show')}}" method="post" target="_blank" >
                                                @csrf
                                                <input type="text" value="{{$room->id}}" name="room_id" hidden>
                                                <input type="text" value="{{$hotel->id}}"  name="hotel_id" hidden>
                                                <input type="date" value="{{$checkin}}" name="checkin" hidden>
                                                <input type="date" value="{{$checkout}}" name="checkout" hidden>
                                                <input class="btn btn-warning text-dark" type="submit" value="Show Details">

                                            </form>
                                            <form style="display: inline" action="{{route('customer.booking.index')}}" method="post" >
                                                @csrf
                                                <input type="text" value="{{$room->id}}" name="room_id" hidden>
                                                <input type="text" value="{{$hotel->id}}"  name="hotel_id" hidden>
                                                <input type="date" value="{{$checkin}}" name="checkin" hidden>
                                                <input type="date" value="{{$checkout}}" name="checkout" hidden>
                                                <input class="btn btn-success text-light" type="submit" value="Book This Room">

                                            </form>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endforeach


                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')
    <script src="{{asset('ui/frontend/plugins/Isotope/isotope.pkgd.min.js')}}"></script>
    <script src="{{asset('ui/frontend/js/destinations.js')}}"></script>
@endpush
