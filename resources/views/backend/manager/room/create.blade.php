@extends('backend.layouts.master_tem')
@section('title','Manager: Room Create')

@section('content')

    <div class="container-fluid">


        <h3 style="text-align:center">Create Room | Please Enter the Room Information and Facilities</h3>
        <div class="container" style="margin-bottom: 20px">

            {!! Form::open(['route'=>'rooms.store','files' => true]) !!}

                @include('backend.manager.room.form')

            <div class="form-row ">

                <div class="form-group col-6 text-center" >
                    <strong> <label for="photo">Room Picture</label> </strong>
                    <input type="file" class="form-control" name="photo" id="photo" required>
                </div>
            </div>

            <div class="form-row">
                <div class="col-4 text-center"></div>
                <div class="col-4 text-center">
                    {!! Form::submit('Add Your Hotel Room',['class'=>['btn','btn-primary'] ]) !!}
                </div>
                <div class="col-4 text-center"></div>

            </div>

            {!! Form::close() !!}
        </div>
    </div>
@endsection
