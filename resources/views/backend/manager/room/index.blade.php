@extends('backend.layouts.master_tem')
@section('title','Manager: Room List')

@section('title', 'Manager: Rooms List')

@section('content')

    <h1 class="h3 mb-2 text-gray-800" style="text-align:center;">List of Rooms</h1>


    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6   class="m-0 font-weight-bold text-primary"><a href="{{ route('rooms.create') }}" style="color:white;" class="btn btn-primary">Add Room</a></h6>
        </div>
        @if(count($rooms) == 0)
            <h3>No Room Has Found. Add some Rooms</h3>
        @else
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Room No.</th>
                            <th>Type</th>
                            <th>Single Beds</th>
                            <th>Double Beds</th>
                            <th>price(BDT)</th>
                            <th>Actions</th>
                        </tr>
                        </thead>

                        <tbody>
                        @php
                            $i = 0
                        @endphp
                        @foreach($rooms as $room)
                            <tr>
                                <td> {{++$i}}</td>
                                <td>{{$room->number}}</td>
                                <td>{{$room->type}}</td>
                                <td>{{$room->single_beds}}</td>
                                <td>{{$room->double_beds}}</td>
                                <td>{{$room->price}}</td>
                                <td >
                                    <a href="{{ route('rooms.show', [$room->id]) }}" style="color:black;" class="btn btn-info">
                                        Details
                                    </a>
                                    <a href="{{ route('rooms.edit', [$room->id]) }}" style="color:black;" class="btn btn-warning">
                                        Edit
                                    </a>

                                    <form action="{{ route('rooms.destroy', [$room->id]) }}" method="post" style="display: inline">
                                        @csrf
                                        @method('delete')
                                        <button type="submit"  class="btn btn-danger" disabled onclick="return confirm('Are you sure want to delete ?')">Delete</button>
                                    </form>

                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        @endif
    </div>


@endsection
