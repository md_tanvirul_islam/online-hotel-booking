@extends('backend.layouts.master_tem')
@section('title','Manager: Room Update')

@section('content')

    <div class="container-fluid">


        <h1 style="text-align:center">Enter Your Hotel Information and Complete the Hotel Profile</h1>

        <div class="container" style="margin-bottom: 20px">

            {!! Form::model($room,[
                                 'route'=>['rooms.update',[$room->id]],
                                 'method' => 'put','files' => true
                                 ]) !!}
                @include('backend.manager.room.form')

            <div class="form-row">
                <div class="form-group col-8">
                    @if(strpos("$room->photo","images.unsplash.com/"))
                        <img src="{{$room->photo}}" style="width: 100%;height:400px" alt="">
                    @else
                        <img src='{{asset("uploads/rooms/$room->photo")}}' style="width: 100%;height:400px" alt="">
                    @endif
                </div>

                <div class="form-group col-4" style="padding-top: 300px">
                    <strong> <label for="photo" class="text-danger">If you want to change the Room picture:</label></strong>
                    <input type="file" id="photo" name="photo" class="form-control">
                </div>
            </div>
                <div class="form-row">
                    <div class="col-4 text-center"></div>
                    <div class="col-4 text-center">
                        {!! Form::submit('Update The Room Info',['class'=>['btn','btn-primary'] ]) !!}
                    </div>
                    <div class="col-4 text-center"></div>

                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
