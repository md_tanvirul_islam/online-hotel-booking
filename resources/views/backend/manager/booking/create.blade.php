@extends('backend.layouts.master_tem')
@section('title','Manager: Room Create')

@section('content')

    <div class="container-fluid">


        <h3 style="text-align:center">Create Room |Please fill these information to create booking</h3>

        <div class="container" style="margin-bottom: 20px">

            {!! Form::open(['route'=>'bookings.store']) !!}

                @include('backend.manager.booking.form')

            <div class="form-row">
                <div class="col-4 text-center"></div>
                <div class="col-4 text-center">
                    {!! Form::submit('Create Booking',['class'=>['btn','btn-primary'] ]) !!}
                </div>
                <div class="col-4 text-center"></div>

            </div>

            {!! Form::close() !!}
        </div>
    </div>
@endsection
