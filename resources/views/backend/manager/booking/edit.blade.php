@extends('backend.layouts.master_tem')
@section('title','Manager: Room Update')

@section('content')

    <div class="container-fluid">


        <h1 style="text-align:center">Enter Your Hotel Information and Complete the Hotel Profile</h1>

        <div class="container" style="margin-bottom: 20px">

            {!! Form::model($room,[
                                 'route'=>['rooms.update',[$room->id]],
                                 'method' => 'put'
                                 ]) !!}
                @include('backend.manager.room.form')
                <div class="form-row">
                    <div class="col-4 text-center"></div>
                    <div class="col-4 text-center">
                        {!! Form::submit('Update The Room Info',['class'=>['btn','btn-primary'] ]) !!}
                    </div>
                    <div class="col-4 text-center"></div>

                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
