@extends('backend.layouts.master_tem')
@section('title','Manager: Room Details')
@section('content')
    <h3 class="h3 mb-2 text-gray-800" style="text-align:center;">Room Info | {{$hotel->name}}</h3>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6   class="m-0 font-weight-bold text-primary">
                <a href="{{ route('rooms.index' )}}" style="color:black;font-size: large" class="btn btn-info">List of Room</a>
                <a href="{{ route('rooms.edit',[$room->id]) }}" style="color:black;font-size: large" class="btn btn-warning">Edit the Room Info </a>
            </h6>
        </div>

        <div class="container" style="margin-bottom: 20px">

            {!! Form::model($room) !!}

                @include('backend.manager.room.form')

            {!! Form::close() !!}
        </div>

    </div>

@endsection
