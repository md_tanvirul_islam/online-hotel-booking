@extends('backend.layouts.master_tem')

@section('content')

    <div class="container-fluid">


        <h1 style="text-align:center">Enter Your Hotel Information and Complete the Hotel Profile</h1>

        <div class="container" style="margin-bottom: 20px">

            {!! Form::open(['route'=>'hotels.store','files' => true]) !!}

                <div class="form-row">
                    <div class="form-group col-7">
                        {!! Form::label('name','Hotel name:') !!}
                        {!! Form::text('name',null,['class'=>'form-control','required']) !!}
                    </div>

                    <div class="form-group col-3">
                        <label for="photo">Hotel Picture</label>
                        <input type="file" class="form-control" name="photo" id="photo" required>

                    </div>

                    <div class="form-group col-2">
                        {!! Form::label('star','Hotel Star:') !!}
                        {!! Form::number('star',null,['class'=>'form-control','required']) !!}
                    </div>
                </div>

                @include('backend.manager.hotel.form')

            {!! Form::close() !!}
        </div>
    </div>
@endsection
