@extends('backend.layouts.master_tem')

@section('title', 'Manager: User List')

@section('content')

    <h1 class="h3 mb-2 text-gray-800" style="text-align:center;">List of Customer</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
{{--            <h6   class="m-0 font-weight-bold text-primary"><a href="{{ route('users.create') }}" style="color:white;" class="btn btn-primary">Create User</a>--}}

{{--            </h6>--}}
        </div>
        @if(count($bookings) == 0)
            <h3>No Record Found. Add some records</h3>
        @else

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead class="thead-dark">
                    <tr>
                        <th style="text-align: center">No.</th>
                        <th style="text-align: center">name</th>
                        <th style="text-align: center">Email</th>
                        <th  style="text-align: center">Phone</th>
                        <th style="text-align: center">Actions</th>
                    </tr>
                    </thead>

                    <tbody>
                    @php
                        $i = 0
                    @endphp
                    @foreach($bookings as $booking)
                        @php
                        $user = \App\User::findorFail($booking->user_id);
                        @endphp
                        <tr>
                            <td style="text-align: center"> {{++$i}}</td>
                            <td style="text-align: center">{{ $user->name }}</td>
                            <td style="text-align: center">{{ $user->email }}</td>

                            <td style="text-align: center">+88 0191 111 2222</td>

                            <td style="text-align: center">
                                <a disabled href="{{ route('users.show', [$user->id]) }}" style="color:black;" class="btn btn-warning">
                                   Profile
                                </a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
        @endif
    </div>


@endsection
