<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Bookings</title>

{{--    <!-- Custom fonts for this template-->--}}
    <link href="{{asset('ui/backend/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('ui/backend/plugins/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('ui/backend/css/main_styles.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('ui/backend/css/responsive.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('ui/backend/plugins/OwlCarousel2-2.2.1/owl.carousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('ui/backend/plugins/OwlCarousel2-2.2.1/owl.theme.default.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('ui/backend/plugins/OwlCarousel2-2.2.1/animate.css') }}">
    <link href="{{asset('ui/backend/css/sb-admin-2.min.css') }}" rel="stylesheet">
    <link href="{{asset('ui/backend/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <style>
        td {
            text-align: center;
            vertical-align: middle;
        }
    </style>

</head>
<body>
<div class="super_container">
    <header>
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{--                    {{ config('app.name', 'Laravel') }}--}}
                    Travelo
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else

                            <li class="nav-item active">
                                <a class="nav-link" href="#">Rooms </a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="#">Bookings</a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="#">Payments</a>
                            </li>


                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    @yield('content')


{{--    !--footer  start-->--}}

    <footer class="footer">
        <div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="{{asset('ui/frontend/images/footer_1.jpg')}}" data-speed="0.8"></div>
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="newsletter">
                        <div class="newsletter_title_container text-center">
                            <div class="newsletter_title">Subscribe to our newsletter to get the latest trends & news</div>
                            <div class="newsletter_subtitle">Join our database NOW!</div>
                        </div>
                        <div class="newsletter_form_container">
                            <form action="#" class="newsletter_form d-flex flex-md-row flex-column align-items-start justify-content-between" id="newsletter_form">
                                <div class="d-flex flex-md-row flex-column align-items-start justify-content-between">
                                    <div><input type="text" class="newsletter_input newsletter_input_name" id="newsletter_input_name" placeholder="Name" required="required"><div class="input_border"></div></div>
                                    <div><input type="email" class="newsletter_input newsletter_input_email" id="newsletter_input_email" placeholder="Your e-mail" required="required"><div class="input_border"></div></div>
                                </div>
                                <div><button class="newsletter_button">subscribe</button></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row footer_contact_row">
                <div class="col-xl-10 offset-xl-1">
                    <div class="row">

                        <!-- Footer Contact Item -->
                        <div class="col-xl-4 footer_contact_col">
                            <div class="footer_contact_item d-flex flex-column align-items-center justify-content-start text-center">
                                <div class="footer_contact_icon"><img src="{{asset('ui/frontend/images/sign.svg')}}" alt=""></div>
                                <div class="footer_contact_title">give us a call</div>
                                <div class="footer_contact_list">
                                    <ul>
                                        <li>Office Landline: +44 5567 32 664 567</li>
                                        <li>Mobile: +44 5567 89 3322 332</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <!-- Footer Contact Item -->
                        <div class="col-xl-4 footer_contact_col">
                            <div class="footer_contact_item d-flex flex-column align-items-center justify-content-start text-center">
                                <div class="footer_contact_icon"><img src="{{asset('ui/frontend/images/trekking.svg')}}" alt=""></div>
                                <div class="footer_contact_title">come & drop by</div>
                                <div class="footer_contact_list">
                                    <ul style="max-width:190px">
                                        <li>4124 Barnes Street, Sanford, FL 32771</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <!-- Footer Contact Item -->
                        <div class="col-xl-4 footer_contact_col">
                            <div class="footer_contact_item d-flex flex-column align-items-center justify-content-start text-center">
                                <div class="footer_contact_icon"><img src="{{asset('ui/frontend/images/around.svg')}}" alt=""></div>
                                <div class="footer_contact_title">send us a message</div>
                                <div class="footer_contact_list">
                                    <ul>
                                        <li>youremail@gmail.com</li>
                                        <li>Office@yourbusinessname.com</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col text-center"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --> </div>
    </footer>

</div>


<script src="{{asset('ui/backend/js/jquery-3.5.1.js' )}}"></script>
<script src="{{asset('ui/backend/js/popper.js' )}} "></script>
<script src="{{asset('ui/backend/js/bootstrap.js' )}} "></script>
<script src="{{asset('ui/backend/plugins/OwlCarousel2-2.2.1/owl.carousel.js' )}} "></script>
<script src="{{asset('ui/backend/plugins/Isotope/isotope.pkgd.min.js' )}} "></script>
<script src="{{asset('ui/backend/plugins/scrollTo/jquery.scrollTo.min.js' )}} "></script>
<script src="{{asset('ui/backend/plugins/easing/easing.js' )}} "></script>
<script src="{{asset('ui/backend/plugins/parallax-js-master/parallax.min.js')}} "></script>
<script src="{{asset('ui/backend/js/custom.js' )}} "></script>
<script src="{{asset('ui/backend/js/all.min.js' )}}"></script>
<!-- Page level plugins -->
<script src="{{asset('ui/backend/datatables/jquery.dataTables.js' )}}"></script>
<script src="{{asset('ui/backend/datatables/dataTables.bootstrap4.js' )}}"></script>

<!-- Page level custom scripts -->
<script src="{{asset('ui/backend/datatables/datatables-demo.js' )}}"></script>

</body>
</html>

