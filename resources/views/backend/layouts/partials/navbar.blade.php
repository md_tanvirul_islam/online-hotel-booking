<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    @auth
        @php
            $user = \Illuminate\Support\Facades\Auth::user();
            $role = \App\Role::where('id','=',"$user->role_id")->first();
        @endphp

{{--    manager checking--}}
        @if($role->name == "manager")
                <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route('manager.dashboard.index')}}">
                    <div class="sidebar-brand-icon rotate-n-15">
                            <i class="fas fa-moon" ></i>
                    </div>
                    <div class="sidebar-brand-text mx-3">Manager Dashboard</div>
                </a>

{{--            admin checking--}}
        @elseif($role->name == "admin")
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route('admin.dashboard.index')}}">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-moon" ></i>
                </div>
                <div class="sidebar-brand-text mx-3">Admin Dashboard</div>
            </a>
        @endif

    @endauth

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    @auth

        @if($role->name == "manager")
            <li class="nav-item active">
                <a class="nav-link" href="{{route('manager.dashboard.index')}}">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span>
                </a>
            </li>
        @elseif($role->name == "admin")
            <li class="nav-item active">
                <a class="nav-link" href="{{route('admin.dashboard.index')}}">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span>
                </a>
            </li>
    @endif

@endauth

{{--    nav item dashboard end--}}

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Interface
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-cog"></i>
            <span>All Tables</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Tables</h6>
                @guest
                @else
                    @if($role->name == "manager")

                        <a class="collapse-item" href="{{route('hotels.index')}}">My Hotel </a>

                        <a class="collapse-item" href="{{route('rooms.index')}}">Rooms </a>

                        <a class="collapse-item" href="{{route('bookings.index')}}">Bookings</a>

                        <a class="collapse-item" href="#">Payments</a>

                    @elseif($role->name == "admin")

                        <a class="collapse-item" href="{{route('roles.index')}}">Roles </a>

                        <a class="collapse-item" href="{{route('users.index')}}">Users </a>

                        <a class="collapse-item" href="{{route('divisions.index')}}">Divisions </a>

                        <a class="collapse-item" href="{{route('districts.index')}}">Districts </a>

                        <a class="collapse-item" href="{{route('hotel_lists.index')}}">Hotels </a>

                        <a class="collapse-item" href="{{route('room_lists.index')}}">Rooms </a>

                        <a class="collapse-item" href="{{route('admin.booking.index')}}">Bookings </a>
                    @endif
                @endguest
            </div>
        </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    @guest
    @else
        @if($role->name == "manager")
            <li class="nav-item">
                <a class="nav-link" href="{{route('hotels.index')}}">
                    <i class="fa fa-h-square" aria-hidden="true"></i>
                    <span>My Hotel</span></a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{route('rooms.index')}}">
                    <i class="fas fa-bed"></i>
                    <span>Rooms</span></a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{route('bookings.index')}}">
                    <i class="fas fa-save"></i>
                    <span>Bookings</span></a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{route('manager.customer.index')}}">
                    <i class="fas fa-users"></i>
                    <span>customer</span></a>
            </li>

        @elseif($role->name == "admin")
            <li class="nav-item">
                <a class="nav-link" href="{{route('roles.index')}}">
                    <i class="fas fa-registered"></i>
                    <span>Roles</span></a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{route('users.index')}}">
                    <i class="fas fa-users"></i>
                    <span>Users</span></a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{route('divisions.index')}}">
                    <i class='fas fa-directions'></i>
                    <span>Divisions</span></a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{route('districts.index')}}">
                    <i class="fas fa-compass"></i>
                    <span>Districts</span></a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{route('hotel_lists.index')}}">
                    <i class="fa fa-h-square" aria-hidden="true"></i>
                    <span>Hotels</span></a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{route('room_lists.index')}}">
                    <i class="fas fa-bed"></i>
                    <span>Rooms</span></a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{route('admin.booking.index')}}">
                    <i class="fas fa-save"></i>
                    <span>Bookings</span></a>
            </li>
    @endif
@endguest

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->
