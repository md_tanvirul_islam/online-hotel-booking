<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        th,td
        {
            text-align: center;
        }
    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
{{--                    {{ config('app.name', 'Laravel') }}--}}
                    Travelo
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->

                    <ul class="navbar-nav mr-auto">
                        @auth
                            @php
                                $user = \Illuminate\Support\Facades\Auth::user();
                                $role = \App\Role::where('id','=',"$user->role_id")->first();
                            @endphp
                            @if($role->name == "manager")
                                <li class="nav-item active">
                                    <a class="nav-link" href="{{route('manager.dashboard.index')}}">Dashboard </a>
                                </li>
                            @elseif($role->name == "admin")
                                <li class="nav-item active">
                                    <a class="nav-link" href="{{route('admin.dashboard.index')}}">Dashboard </a>
                                </li>
                            @endif

                        @endauth

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            @if($role->name == "manager")
                            <li class="nav-item active">
                                <a class="nav-link" href="{{route('hotels.index')}}">My Hotel </a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="{{route('rooms.index')}}">Rooms </a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="#">Bookings</a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="#">Payments</a>
                            </li>
                            @elseif($role->name == "admin")
                                <li class="nav-item active">
                                    <a class="nav-link" href="{{route('roles.index')}}">Roles </a>
                                </li>
                                <li class="nav-item active">
                                    <a class="nav-link" href="{{route('users.index')}}">Users </a>
                                </li>
                                <li class="nav-item active">
                                    <a class="nav-link" href="{{route('divisions.index')}}">Divisions </a>
                                </li>
                                <li class="nav-item active">
                                    <a class="nav-link" href="{{route('districts.index')}}">Districts </a>
                                </li>
                                <li class="nav-item active">
                                    <a class="nav-link" href="{{route('hotel_lists.index')}}">Hotels </a>
                                </li>
                                <li class="nav-item active">
                                    <a class="nav-link" href="{{route('room_lists.index')}}">Rooms </a>
                                </li>
                            @endif


                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @include('frontend.message')
            @include('backend.layouts.flash-message')
            @yield('content')
        </main>
    </div>
</body>
</html>
