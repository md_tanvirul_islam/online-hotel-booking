@extends('backend.layouts.master_tem')

@section('content')
    <div class="container">

        <div class="container" style="margin-bottom: 20px">
            <div style="text-align: justify">
                <a class="btn btn-secondary" href="{{route('districts.index')}}">List of Districts</a>
            </div>
            <h1 style="text-align:center;margin-bottom: 40px">Edit the info of The Districts</h1>

            @include('backend.layouts.errors')

            {!! Form::model($district,[
                            'route'=>['districts.update',$district->id],
                            'method' => 'put'
                            ]) !!}
            <div class="form-row">
                <div class="col-4 text-center ">
                    <h4> {!! Form::label('name','Name:') !!} </h4>
                </div>

                <div class="form-group col-4 text-center" >
                    <h4> {!! Form::label('division_id','Division :') !!} </h4>
                </div>
            </div>
            <div class="form-row">

                <div class="col-4 text-center ">

                    {!! Form::text('name',null,['class'=>'form-control','required']) !!}

                </div>
                <div class="form-group col-4">
                    <select name="division_id" class="form-control" required id="division_id">
                        <option value="{{$divisionOfThisDistrict->id}}">{{$divisionOfThisDistrict->name}}</option>
                        @foreach($divisions as $division)
                            <option value="{{$division->id}}">{{$division->name}}</option>
                        @endforeach
                    </select>
                </div>

            <div class="col-4 text-left">
                {!! Form::submit('Update',['class'=>['btn','btn-primary'] ]) !!}
            </div>

        </div>

        {!! Form::close() !!}

    </div>

@endsection

