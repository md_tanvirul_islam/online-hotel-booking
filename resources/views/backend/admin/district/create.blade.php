@extends('backend.layouts.master_tem')

@section('content')
    <div class="container">

        <div class="container" style="margin-bottom: 20px">
            <div style="text-align: justify">
                <a class="btn btn-secondary" href="{{route('districts.index')}}">List of Districts</a>
            </div>
            <h1 style="text-align:center;margin-bottom: 40px">Enter the Name of The Districts</h1>

            @include('backend.layouts.errors')

            {!! Form::open(['route'=>'districts.store']) !!}

            @include('backend.admin.district.form')


                <div class="col-4 text-left">
                    {!! Form::submit('Add',['class'=>['btn','btn-primary'] ]) !!}
                </div>

            </div>

            {!! Form::close() !!}

        </div>

@endsection
