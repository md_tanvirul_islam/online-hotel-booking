@extends('backend.layouts.master_tem')

@section('title', 'Division List')

@section('content')

    <h1 class="h3 mb-2 text-gray-800" style="text-align:center;">List of Districts in Bangladesh</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6   class="m-0 font-weight-bold text-primary"><a href="{{ route('districts.create') }}" style="color:white;" class="btn btn-primary">Add District</a></h6>
        </div>
        @if(count($districts) == 0)
            <h3>No Record Found. Add some records</h3>
        @else
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>name</th>
                        <th>Division</th>
                        <th>Actions</th>
                    </tr>
                    </thead>

                    <tbody>
                    @php
                    $i = 0
                    @endphp
                    @foreach($districts as $district)
                    <tr>
                        <td> {{++$i}}</td>
                        <td>{{ $district->name }}</td>
                        @php
                        $id = $district->id;
                        $division= \App\LocationDistrict::find($id)->division;
                        @endphp
                        <td>{{$division->name}}</td>

                        <td >


                            <a href="{{ route('districts.edit', [$district->id]) }}" style="color:black;" class="btn btn-warning">
                                Edit
                            </a>

                            <form action="{{ route('districts.destroy', [$district->id]) }}" method="post" style="display: inline">
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure want to delete ?')">Delete</button>
                            </form>

                        </td>
                    </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
        @endif
    </div>






























{{--            <h6 class="m-0 font-weight-bold text-primary float-left">List</h6>--}}
{{--            <a class="btn btn-primary float-right" href="{{ route('divisions.create') }}">Create</a>--}}


{{--            @include('layouts.message')--}}

{{--            <div class="table-responsive">--}}
{{--                <table class="table table-bordered"  width="100%" cellspacing="0">--}}
{{--                    <thead>--}}
{{--                    <tr>--}}
{{--                        --}}

{{--                    </tr>--}}
{{--                    </thead>--}}
{{--                    <tbody>--}}
{{--                    @foreach($divisions as $division)--}}
{{--                        <tr>--}}
{{--                            <td>{{ $division->name }}</td>--}}
{{--                           <td>--}}
{{--                                <a href="{{ route('divisions.show', [$division->id]) }}" class="btn btn-info">Show</a>--}}
{{--                                <a href="{{ route('divisions.edit', [$division->id]) }}" class="btn btn-warning">Edit</a>--}}

{{--                                <form action="{{ route('divisions.destroy', [$division->id]) }}" method="post" style="display: inline">--}}
{{--                                    @csrf--}}
{{--                                    @method('delete')--}}
{{--                                    <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure want to delete ?')">Delete</button>--}}
{{--                                </form>--}}

{{--                            </td>--}}
{{--                        </tr>--}}
{{--                    @endforeach--}}
{{--                    </tbody>--}}
{{--                </table>--}}
{{--            </div>--}}

{{--    </div>--}}
@endsection
