<div class="form-row">
    <div class="col-4 text-center ">
        <h4> {!! Form::label('name','Name:') !!} </h4>
    </div>

    <div class="form-group col-4 text-center" >
        <h4> {!! Form::label('division_id','Division :') !!} </h4>
    </div>
</div>

<div class="form-row">

    <div class="col-4 text-center ">

        {!! Form::text('name',null,['class'=>'form-control','required']) !!}

    </div>
    <div class="form-group col-4">
        <select name="division_id" class="form-control" required id="division_id">
            <option value="#">---------</option>
            @foreach($divisions->all() as $division)
                <option value="{{$division->id}}">{{$division->name}}</option>
            @endforeach
        </select>
    </div>


