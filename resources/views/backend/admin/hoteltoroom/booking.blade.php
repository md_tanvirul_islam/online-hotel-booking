@extends('backend.layouts.master_tem')
@section('title','Admin: Room to Booking List')
@section('content')

    <h1 class="h3 mb-2 text-gray-800" style="text-align:center;">List of Bookings</h1>


    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
{{--            <h6   class="m-0 font-weight-bold text-primary"><a href="{{ route('rooms.create') }}" style="color:white;" class="btn btn-primary">Add Room</a></h6>--}}
        </div>
        @if(count($bookings) == 0)
            <h3 class="text-danger">No Booking Find.</h3>
        @else
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Room No.</th>
                            <th>Room Type.</th>
                            <th>Customer Name </th>
                            <th>Email </th>
                            <th>phone </th>
                            <th>Check In</th>
                            <th>Check Out</th>
                            <th>No. Days</th>
                            <th>Amount [BDT]</th>
{{--                            <th>Action</th>--}}
{{--                            <th>Pay Status</th>--}}
{{--                            <td class="text-info">Click After Pay</td>--}}

                        </tr>
                        </thead>

                        <tbody>
                        @php
                            $i = 0
                        @endphp
                        @foreach($bookings as $booking)
                            <tr>
                                <td> {{++$i}}</td>
                                @php
                                    $room= \App\Room::findorFail($booking->room_id);
                                @endphp
                                <td>{{$room->number}}</td>
                                <td>{{$room->type}}</td>
                                @php
                                    $customer= \App\User::findorFail($booking->user_id);
                                @endphp
                                <td>{{$customer->name}}</td>
                                <td>{{$customer->email}}</td>
                                <td>{{$customer->phone}}</td>
                                <td>{{$booking->checkin}}</td>
                                <td>{{$booking->checkout}}</td>
                                <td>{{$booking->days}}</td>
                                <td>{{$booking->cost}}</td>

{{--                                <td >--}}
{{--                                    <a href="{{ route('bookings.show', [$booking->id]) }}" disabled style="color:black;" class="btn btn-info">--}}
{{--                                        <i class="fa fa-eye"  title="Details"></i>--}}
{{--                                    </a>--}}
{{--                                    <a href="{{ route('bookings.edit', [$booking->id]) }}" disabled style="color:black;" class="btn btn-warning">--}}
{{--                                        <i class="fa fa-edit"  title="Edit"></i>--}}
{{--                                    </a>--}}

{{--                                    <form action="{{ route('bookings.destroy', [$booking->id]) }}"  method="post">--}}
{{--                                        @csrf--}}
{{--                                        @method('delete')--}}
{{--                                        <button type="submit"  class="btn btn-danger" disabled onclick="return confirm('Are you sure want to delete ?')">--}}
{{--                                            <i class="far fa-trash-alt" title="Delete"></i>--}}
{{--                                        </button>--}}
{{--                                    </form>--}}

{{--                                </td>--}}
{{--                                <td>--}}
{{--                                    @if($booking->paid)--}}
{{--                                        <i class="far fa-check-square" style="font-size:48px;color:lawngreen" aria-hidden="true"></i>--}}
{{--                                    @else--}}
{{--                                        <i class="fa fa-times" style="font-size:48px;color:red" aria-hidden="true"></i>--}}
{{--                                    @endif--}}
{{--                                </td>--}}
{{--                                <td>--}}
{{--                                    @if($booking->paid)--}}
{{--                                        <span class="text-success font-weight-bold">Done</span>--}}
{{--                                    @else--}}
{{--                                        <a href="{{ route('bookings.pay', [$booking->id]) }}" class="btn btn-info text-dark"><i class="fas fa-check-circle"></i></a>--}}
{{--                                    @endif--}}
{{--                                </td>--}}
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        @endif
    </div>


@endsection
