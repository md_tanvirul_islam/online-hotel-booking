@extends('backend.layouts.master_tem')
@section('title','Admin: Hotel to Room Create')

@section('content')

    <div class="container-fluid">

        <h2 style="text-align:center">{{$hotel->name}}</h2>
        <h4 style="text-align:left;margin-left: 20px" >Please Enter the Room Information and Facilities:</h4>


        <div class="container" style="margin-bottom: 20px">

            {!! Form::open(['route'=>['Admin.hotel.rooms.store',$id]]) !!}

                @include('backend.admin.hoteltoroom.form')

            <div class="form-row">
                <div class="col-4 text-center"></div>
                <div class="col-4 text-center">
                    {!! Form::submit('Add Your Hotel Room',['class'=>['btn','btn-primary'] ]) !!}
                </div>
                <div class="col-4 text-center"></div>

            </div>

            {!! Form::close() !!}
        </div>
    </div>
@endsection
