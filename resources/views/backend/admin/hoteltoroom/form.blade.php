
<div class="form-row">
    <div class="form-group col-3">
        {!! Form::label('number','Room Number:') !!}
        {!! Form::text('number',null,['class'=>'form-control','required']) !!}
    </div>

    <div class="form-group col-3">
        {!! Form::label('type','Room Type:') !!}
        {!! Form::text('type',null,['class'=>'form-control','required']) !!}

    </div>

    <div class="form-group col-3">
        {!! Form::label('single_beds','How many single bed?') !!}
        {!! Form::number('single_beds',null,['class'=>'form-control','required','min'=>0]) !!}

    </div>

    <div class="form-group col-3">
        {!! Form::label('double_beds','How many double bed?') !!}
        {!! Form::number('double_beds',null,['class'=>'form-control','required','min'=>0]) !!}

    </div>
</div>

<div style="margin-top: 20px">
    <strong style="padding: 5px">Room Facilities</strong>
    <div class="form-row ">
            <div class="form-group col-3 text-center" >
                <h5>Television</h5>
                {!! Form::label('TV','yes') !!}
                {!! Form::radio('TV', '1') !!}<br>
                {!! Form::label('TV','No') !!}
                {!! Form::radio('TV', '0') !!}
            </div>

        <div class="form-group col-3 text-center" >
            <h5>Air Conditioner</h5>
            {!! Form::label('AC','yes') !!}
            {!! Form::radio('AC', '1') !!}<br>
            {!! Form::label('AC','No') !!}
            {!! Form::radio('AC', '0') !!}
        </div>

        <div class="form-group col-3 text-center" >
            <h5>Bathtub to take Bath</h5>
            {!! Form::label('bathtub','yes') !!}
            {!! Form::radio('bathtub', '1') !!}<br>
            {!! Form::label('bathtub','No') !!}
            {!! Form::radio('bathtub', '0') !!}
        </div>

        <div class="form-group col-3 text-center" >
            <h5>Water Heater</h5>
            {!! Form::label('water_heater','yes') !!}
            {!! Form::radio('water_heater', '1') !!}<br>
            {!! Form::label('water_heater','No') !!}
            {!! Form::radio('water_heater', '0') !!}
        </div>
    </div>
    <div class="form-group form-row">

        <div class="form-group col-4 text-center" >
            <h5>Refrigerator for Foods and Drinks</h5>
            {!! Form::label('refrigerator','yes') !!}
            {!! Form::radio('refrigerator', '1') !!}<br>
            {!! Form::label('refrigerator','No') !!}
            {!! Form::radio('refrigerator', '0') !!}
        </div>

        <div class="form-group col-4 text-center" >
            <h5>Machine for Coffee/Tea making</h5>
            {!! Form::label('coffee_tea_maker','yes') !!}
            {!! Form::radio('coffee_tea_maker', '1') !!}<br>
            {!! Form::label('coffee_tea_maker','No') !!}
            {!! Form::radio('coffee_tea_maker', '0') !!}
        </div>

        <div class="form-group col-4 text-center" >
            <h5>Free Wifi</h5>
            {!! Form::label('wifi','yes') !!}
            {!! Form::radio('wifi', '1') !!}<br>
            {!! Form::label('wifi','No') !!}
            {!! Form::radio('wifi', '0') !!}
        </div>

    <div class="form-row ">

        <div class="form-group col-6 text-center" >
            <strong>{!! Form::label('other_facilities','Details of other Facilities:') !!}</strong>
            {!! Form::textarea('other_facilities',null,['class'=>'form-control','rows'=>5,'placeholder'=>"If your hotel's room have other facilities,you can list them here."]) !!}

        </div>

        <div class="form-group col-4">
            <strong>{!! Form::label('price','Cost per Day in Bangladeshi Taka') !!}</strong>
            {!! Form::number('price',null,['class'=>'form-control','required','min'=>0]) !!}
        </div>

    </div>
</div>
    <input type="text" name="hotel_id" value="{{$hotel->id}}" hidden>


</div>
