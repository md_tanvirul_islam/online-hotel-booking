@extends('backend.layouts.master_tem')
@section('title' ,'Admin: Room Update')
@section('content')

    <div class="container-fluid">

        <h1 class="h3 mb-2 text-gray-800" style="text-align:center;">{{$hotel->name}}</h1>

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6   class="m-0 font-weight-bold text-primary">
                    <a href="{{ route('room_lists.index' )}}" style="color:black;font-size: large" class="btn btn-info">List of Room</a>
                    <a href="{{ route('room_lists.edit',[$room->id]) }}" style="color:black;font-size: large" class="btn btn-warning">Edit the Room Info </a>
                </h6>
            </div>
        <h1 style="text-align:center">Change The Room info Here if You Want</h1>

        <div class="container" style="margin-bottom: 20px">

            {!! Form::model($room,[
                             'route'=>['room_lists.update',$room->id],
                             'method' => 'put','files' => true
                             ]) !!}
                @include('backend.admin.room.form')
                <div class="form-row">
                    <div class="form-group col-8">
                        @if(strpos("$room->photo","images.unsplash.com/"))
                            <img src="{{$room->photo}}" style="width: 100%;height:400px" alt="">
                        @else
                            <img src='{{asset("uploads/rooms/$room->photo")}}' style="width: 100%;height:400px" alt="">
                        @endif
                    </div>

                    <div class="form-group col-4" style="padding-top: 300px">
                        <strong> <label for="photo" class="text-danger">If you want to change the Room picture:</label></strong>
                        <input type="file" id="photo" name="photo" class="form-control">
                    </div>
                </div>
                <div style="text-align: center">
                    <input type="submit" class="btn btn-primary" value="Update the Room Info">
                </div>


            {!! Form::close() !!}
        </div>
    </div>
@endsection
