@extends('backend.layouts.master_tem')
@section('title','Admin: Room Details')
@section('content')
    <h1 class="h3 mb-2 text-gray-800" style="text-align:center;">{{$hotel->name}}</h1>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6   class="m-0 font-weight-bold text-primary">
                <a href="{{ route('room_lists.index' )}}" style="color:black;font-size: large" class="btn btn-info">List of Room</a>
                <a href="{{ route('room_lists.edit',[$room->id]) }}" style="color:black;font-size: large" class="btn btn-warning">Edit the Room Info </a>
            </h6>
        </div>

        <div class="container" style="margin-bottom: 20px">

            {!! Form::model($room,['action'=>null]) !!}

                @include('backend.admin.room.form')

            <div class="form-row">
                <div class="form-group col-12">
                    @if(strpos("$room->photo","images.unsplash.com/"))
                        <img src="{{$room->photo}}" style="width: 100%;height:400px" alt="">
                    @else
                        <img src='{{asset("uploads/rooms/$room->photo")}}' style="width: 100%;height:400px" alt="">
                    @endif
                </div>
            </div>
            <div class="form-row">
                <div class="col-12" style="text-align: center">
                    <strong>Image : {{$room->number}} </strong>
                </div>
            </div>

            {!! Form::close() !!}
        </div>

    </div>

@endsection
