@extends('backend.layouts.master_tem')
@section('title','Admin: Room Create')

@section('content')

    <div class="container-fluid">


        <h1 style="text-align:center">Please Enter the Room Information and Facilities</h1>
        @php
        $hotel = \App\Hotel::where('id','=',"$id")->first();
        @endphp
        <h3 style="text-align: center;font-family: 'Times New Roman'">{{$hotel->name}} </h3>

        <div class="container" style="margin-bottom: 20px">

            {!! Form::open(['route'=>'room_lists.store']) !!}

            @include('backend.admin.room.form')

            <div class="form-row">
                <div class="col-4 text-center"></div>
                <div class="col-4 text-center">
                    {!! Form::submit('Add Your Hotel Room',['class'=>['btn','btn-primary'] ]) !!}
                </div>
                <div class="col-4 text-center"></div>

            </div>

            {!! Form::close() !!}
        </div>
    </div>
@endsection
