@extends('backend.layouts.master_tem')

@section('title', 'Admin: Room List')

@section('content')

    <h1 class="h3 mb-2 text-gray-800" style="text-align:center;">List of All Rooms</h1>


    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6   class="m-0 font-weight-bold text-primary"></h6>
        </div>
        @if(count($rooms) == 0)
            <h3>No Record Found. Add some records</h3>
        @else
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Room No.</th>
                            <th>Hotel</th>
                            <th>Type</th>
                            <th>Single Beds</th>
                            <th>Double Beds</th>
                            <th>Max Person</th>
                            <th>price(BDT)</th>
                            <th>Actions</th>
                        </tr>
                        </thead>

                        <tbody>
                        @php
                            $i = 0
                        @endphp
                        @foreach($rooms as $room)
                            <tr>
                                <td> {{++$i}}</td>
                                <td>{{$room->number}}</td>
                                @php
                                $hotel = \App\Hotel::where('id','=',"$room->hotel_id")->first();
                                @endphp
                                <td>{{$hotel->name}}</td>
                                <td>{{$room->type}}</td>
                                <td>{{$room->single_beds}}</td>
                                <td>{{$room->double_beds}}</td>
                                <td>{{$room->max_person}}</td>
                                <td>{{$room->price}}</td>
                                <td >
                                    <a href="{{ route('room_lists.show', [$room->id]) }}" style="color:black;" class="btn btn-info">
                                        Details
                                    </a>
                                    <a href="{{ route('room_lists.edit', [$room->id]) }}" style="color:black;" class="btn btn-warning">
                                        Edit
                                    </a>

                                    <form action="{{ route('room_lists.destroy', [$room->id]) }}" method="post" style="display: inline">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" disabled class="btn btn-danger" onclick="return confirm('Are you sure want to delete ?')">Delete</button>
                                    </form>

                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        @endif
    </div>


@endsection
