@php
$userId = auth()->user();
$role = \App\Role::findorFail($userId->role_id);
@endphp

@extends($role->name == "admin"||"manager"?'backend.layouts.master_tem':'frontend.master')


@if($role->name === "customer")
    @push('css')
        <link rel="stylesheet" type="text/css" href="{{asset('ui/frontend/styles/search_show.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('ui/frontend/styles/destinations_responsive.css')}}">
    @endpush
@endif



@section('content')
    <!-- Home -->
    @if($role->name == "customer" )
        <div class="home">
            <div class="background_image" style="background-image:url({{asset('ui/frontend/images/destinations.jpg')}})"></div>
        </div>
    @endif


    <div style="margin-top: 25px">
        <h1 class="text-dark" style="text-align: center">Enter the Info to complete your Profile</h1>
    </div>

    <div class="container text-dark font-weight-bold" style="margin-bottom: 50px;margin-top: 50px">

    <form method="POST" action="{{route('profile.store')}}" enctype="multipart/form-data">
        @csrf
        <div  class="form-row form-group" >
            <div class="col" style="text-align: right;">
                <label style="padding-top:15px" for="birth_date">Your Birth Date : </label>
            </div>
            <div class="col">
                <input class="search_input form-control" id="birth_date" type="date" name="birth_date" style="width: 50%">
            </div>
        </div>

        <div  class="form-row form-group">
            <div class="col" style="text-align: right">
                <label style="padding-top:15px" for="country">Your Country :</label>
            </div>
            <div class="col ">
                <input class="search_input form-control" id="country" type="text" name="country" style="width: 50%">
            </div>
        </div>

        <div  class="form-row form-group">
            <div class="col" style="text-align: right">
                <label style="padding-top:15px" for="city">Your City :</label>
            </div>
            <div class="col">
                <input class="search_input form-control" id="city" type="text" name="city" style="width: 50%">
            </div>
        </div>

        <div  class="form-row form-group">
            <div class="col" style="text-align: right">
                <label style="padding-top:15px" for="local_address">Your Local Address :</label>
            </div>
            <div class="col">
                <input class="search_input form-control" id="local_address" type="text" name="local_address" style="width: 50%">
            </div>
        </div>

        <div  class="form-row form-group">
            <div class="col" style="text-align: right">
                <label style="padding-top:15px" for="nationality">Your Nationality :</label>
            </div>
            <div class="col">
                <input class="search_input form-control" id="nationality" type="text" name="nationality" style="width: 50%">
            </div>
        </div>

        <div  class="form-row form-group">
            <div class="col" style="text-align: right;" >
                <label style="padding-top:15px" for="photo">Upload Your Profile Picture :</label>
            </div>
            <div class="col ">
                <input class="search_input form-control" id="photo" type="file" name="photo" style="width: 50%">
            </div>
        </div>
        @php
        $id = auth()->id();
        @endphp
        <input hidden name="user_id" type="text" value="{{$id}}">
        <div class="form-row">
            <div class="col" style="text-align: center">
                <input type="submit" class="btn btn-info" value="Submit">
            </div>

        </div>

    </form>
    </div>

@endsection
