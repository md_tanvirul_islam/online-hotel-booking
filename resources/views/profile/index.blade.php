@php
    $userId = auth()->user();
    $role = \App\Role::findorFail($userId->role_id);
@endphp

@extends($role->name == "admin"||"manager"?'backend.layouts.master_tem':'frontend.master')


@if($role->name === "customer")
    @push('css')
        <link rel="stylesheet" type="text/css" href="{{asset('ui/frontend/styles/search_show.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('ui/frontend/styles/destinations_responsive.css')}}">
    @endpush
@endif

@section('content')
    <!-- Home -->
    @if($role->name == "customer" )
        <div class="home">
            <div class="background_image" style="background-image:url({{asset('ui/frontend/images/destinations.jpg')}})"></div>
        </div>
    @endif

    <div class="container " style="margin-top: 50px;margin-bottom: 200px">
        <div class="row">
            <div class="col"> </div>
            <div class="col"> <h3 class="text-info" style="text-align: center;margin-bottom: 20px"> {{$user->name}} Profile</h3> </div>
            <div style="text-align: right" class="col"> <a style="color:#000;" href="{{route('profile.edit',[$user->id])}}" class="btn btn-warning">Edit Profile</a> </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="row">
                    <div class="col" style="text-align: center">
                        <img src='{{asset("uploads\profiles")}}\{{$profile->photo}}' alt="" style="height:350px;width:300px;"/>
                    </div>
                </div>

                <div class="row">
                    <div class="col" style="text-align: center">
                         <a style="color: black;margin-top: 5px" href="#" class="btn btn-warning">Change Photo</a><br>
                        <a style="color: black;margin-top: 3px" href="#" class="btn btn-danger">Remove Photo</a>
                    </div>
                </div>

            </div>
            <div class="col" style="font-size: 20px">
                <div class="row">
                    <div class="col">
                        <span class="text-dark font-weight-bold"> Name</span>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <span class="text-dark font-weight-bold"> Email</span>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <span class="text-dark font-weight-bold"> Phone</span>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <span class="text-dark font-weight-bold"> Birth Date</span>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <span class="text-dark font-weight-bold"> Country</span>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <span class="text-dark font-weight-bold"> City</span>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <span class="text-dark font-weight-bold"> Local Address</span>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <span class="text-dark font-weight-bold"> Nationality</span>
                    </div>
                </div>

            </div>
            <div class="col text-dark" style="font-size: 20px">
                <div class="row">
                    <div class="col">
                        <span> {{$user->name}}</span>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <span> {{$user->email}}</span>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <span> {{$user->email}}</span>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <span> {{$profile->birth_date}}</span>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <span> {{$profile->country}}</span>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <span> {{$profile->city}}</span>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <span> {{$profile->local_address}}</span>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <span> {{$profile->nationality}}</span>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

@push('js')

    <script src="{{asset('ui/frontend/plugins/Isotope/isotope.pkgd.min.js')}}"></script>
    <script src="{{asset('ui/frontend/plugins/scrollTo/jquery.scrollTo.min.js')}}"></script>
    <script src="{{asset('ui/frontend/js/custom.js')}}"></script>
@endpush
