@php
    $userId = auth()->user();
    $role = \App\Role::findorFail($userId->role_id);
@endphp

@extends($role->name == "admin"||"manager"?'backend.layouts.master_tem':'frontend.master')


@if($role->name === "customer")
    @push('css')
        <link rel="stylesheet" type="text/css" href="{{asset('ui/frontend/styles/search_show.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('ui/frontend/styles/destinations_responsive.css')}}">
    @endpush
@endif

@section('content')
    <!-- Home -->

    <div class="home">
        <div class="background_image" style="background-image:url({{asset('ui/frontend/images/destinations.jpg')}})">

        </div>
    </div>
    <div class="container " style="margin-top: 50px;margin-bottom: 200px">
        <div class="row">
            <div class="col"> </div>
            <div class="col"> <h3 class="text-info" style="text-align: center;margin-bottom: 20px"> {{$user->name}} Profile</h3> </div>
            <div style="text-align: right" class="col"> <a style="color:#ffffff;" href="{{route('profile.edit',[$user->id])}}" class="btn btn-info">Go to Profile</a> </div>
        </div>
        <div>
            <form method="post" action="">
                <div class="row form-group">
                    <div class="col text-dark font-weight-bolder" style="text-align: right;">
                        <label for="name">Your Name </label>
                    </div>

                    <div class="col ">
                        <input class="form-control" name="name" id="name" value="{{$user->name}}" >
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col text-dark font-weight-bolder" style="text-align: right">
                        <label for="email">Your Email </label>
                    </div>

                    <div class="col">
                        <input class="form-control" name="email" id="email" value="{{$user->email}}" >
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col text-dark font-weight-bolder" style="text-align: right">
                        <label for="phone">Your Phone </label>
                    </div>

                    <div class="col">
                        <input class="form-control" name="phone" id="phone" value="{{$user->phone}}" >
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col text-dark font-weight-bolder" style="text-align: right">
                        <label for="country">Your Country </label>
                    </div>
                    <div class="col ">
                        <input class="form-control" name="country" id="country" value="{{$profile->country}}" >
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col text-dark font-weight-bolder" style="text-align: right">
                        <label for="city">Your City </label>
                    </div>

                    <div class="col ">
                        <input class="form-control" name="city" id="city" value="{{$profile->city}}" >
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col text-dark font-weight-bolder" style="text-align: right">
                        <label for="local_address">Your Local Address </label>
                    </div>

                    <div class="col ">
                        <input class="form-control" name="local_address" id="local_address" value="{{$profile->local_address}}" >
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col text-dark font-weight-bolder" style="text-align: right">
                        <label for="nationality">Your Nationality </label>
                    </div>

                    <div class="col ">
                        <input class="form-control" name="nationality" id="nationality" value="{{$profile->nationality}}" >
                    </div>
                </div>


                <div class="row form-group">
                    <div class="col" style="text-align: center">
                        <a href="" class="btn btn-success"> Update Your Profile</a>
                    </div>

                </div>

            </form>
        </div>


    </div>

@endsection

@if($role->name === "customer")
    @push('js')
        <script src="{{asset('ui/frontend/plugins/Isotope/isotope.pkgd.min.js')}}"></script>
        <script src="{{asset('ui/frontend/plugins/scrollTo/jquery.scrollTo.min.js')}}"></script>
        <script src="{{asset('ui/frontend/js/custom.js')}}"></script>
    @endpush
@endif


