<?php

namespace App\Http\Controllers;

use App\Hotel;
use App\LocationDistrict;
use App\LocationDivision;
use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $manager = Auth::user();
        $hotel = User::findorFail($manager->id)->hotel;
//        dd($hotel);
        if($hotel)
        {
            $division = LocationDivision::findorFail($hotel->division_id);
            $district = LocationDistrict::findorFail($hotel->district_id);
            return view('backend.manager.hotel.index',compact('hotel','district','division'));
        }
        else
        {
          return redirect()->route('hotels.create');
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $divisions = LocationDivision::all();
        $districts = LocationDistrict::all();
        return view('backend.manager.hotel.create',compact("divisions","districts"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $request->validate([
                'user_id' => 'required|',
                'name' => 'required',
                'division_id' => 'required|',
                'district_id' => 'required',
                'local_address' => 'required|',
                'telephone' => 'required',
                'mobile1' => 'required|',
                'mobile2' => 'required',
                'star' => 'required|',

            ]);

            $data = $request->all();

            if ($request->hasFile('photo'))
            {
                $file = $request->file('photo');
                $extension = $file->getClientOriginalExtension();
                $filename = time().'.'.$extension;
                $file->move('uploads/hotels/',$filename);
                $data['photo'] = $filename;
            }
            else{
                return $request;
                $data['photo'] = "";

            }
            Hotel::create($data);
            return redirect()->route('hotels.index')->withStatus('Task was successful!');


        }catch (QueryException $exception)
        {
            return redirect()->back()->withErrors($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function show(Hotel $hotel)
   {
        $id =$hotel;
//       $hotel = App\Hotel::findorFail(1);
        $hotel = Auth::user()->hotel();
        dd($hotel) ;
//        return view('layouts.backend.manager.hotel_profile.index',compact());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function edit(Hotel $hotel)
    {
        $DivsionForThisHotel = LocationDivision::findorFail($hotel->division_id) ;
        $DistrictForThisHotel = LocationDivision::findorFail($hotel->division_id) ;
        $divisions = LocationDivision::all();
        $districts = LocationDistrict::all();
        return view('backend.manager.hotel.edit',compact('hotel','DistrictForThisHotel','DivsionForThisHotel','districts','divisions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hotel $hotel)
    {
        try {

            $request->validate([
                'user_id' => 'required|',
                'name' => 'required',
                'division_id' => 'required|',
                'district_id' => 'required',
                'local_address' => 'required|',
                'telephone' => 'required',
                'mobile1' => 'required|',
                'mobile2' => 'required',
                'star' => 'required|',

            ]);

            $data = $request->all();
//            dd($data);
            if ($request->hasFile('photo'))
            {
                $file = $request->file('photo');
                $extension = $file->getClientOriginalExtension();
                $filename = time().'.'.$extension;
                $file->move('uploads/hotels/',$filename);
                $data['photo'] = $filename;
            }
            else{
                return $request;
                $data['photo'] = "";

            }
            $hotel->update($data);
            return redirect()->route('hotels.index')->withStatus('Task was successful!');


        }catch (QueryException $exception)
        {
            return redirect()->back()->withErrors($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hotel $hotel)
    {
        try {


            //$hotel->delete();
            return redirect()->route('hotels.index')->withStatus('Task was successful!');


        }catch (QueryException $exception)
        {
            return redirect()->back()->withErrors($exception->getMessage());
        }
    }
}
