<?php

namespace App\Http\Controllers;

use App\Profile;
use App\Room;
use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use function GuzzleHttp\Promise\all;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index($id)
    {

        $user = User::findorFail($id);
        $profile = Profile::where('user_id','=',"$id")->first();
        if($profile)
        {
            return view('profile.index',compact('profile','user'));
        }
        else
        {
          return view('profile.create');
        }

    }

//    public function create()
//    {
//
//    }

    public function store(Request $request)
    {
        try {

            $data = $request->all();
            if ($request->hasFile('photo'))
            {
                $file = $request->file('photo');
                $extension = $file->getClientOriginalExtension();
                $filename = time().'.'.$extension;
                $file->move('uploads/profiles/',$filename);
                $data['photo'] = $filename;
            }
            else{
                return $request;
                $data['photo'] = "";

            }
            Profile::create($data);
            return redirect()->route('profile.index',$data['user_id'])->with('success','Profile Information added Successfully.');


        }catch (QueryException $exception)
        {
            return redirect()->back()->with('error',$exception->getMessage());
        }
    }

    public function edit($id)
    {
//        dd($id);
        $user = User::findorFail($id);
        $profile = Profile::where('user_id','=',"$id")->first();
        return view('profile.edit',compact('user','profile'));

    }
}
