<?php

namespace App\Http\Controllers;

use App\LocationDistrict;
use App\LocationDivision;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function index()
    {
        $divisions = LocationDivision::select('id','name')->get();
        $districts = LocationDistrict::select('id','name')->get();
        //dd($districts);
        return view('frontend.elements.index',compact('divisions','districts'));
    }

    public function destinations()
    {
        return view('frontend.elements.destinations');
    }

    public function about()
    {
        return view('frontend.elements.about');
    }

    public function contract()
    {
        return view('frontend.elements.contract');
    }

    public function elements()
    {
        return view('frontend.elements.elements');
    }

    public function news()
    {
        return view('frontend.elements.news');
    }
}
