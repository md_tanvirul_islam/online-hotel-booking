<?php

namespace App\Http\Controllers;

use App\LocationDistrict;
use App\LocationDivision;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class LocationDistrictController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $districts = LocationDistrict::orderBy('created_at', 'desc')->get();
        return view('backend.admin.district.index',compact('districts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $divisions = LocationDivision::all();

        return view('backend.admin.district.create',compact('divisions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $request->validate([
                'name' => 'required|unique:location_districts|max:100',

            ]);

            $data = $request->all();
            LocationDistrict::create($data);
            return redirect()->route('districts.index')->withStatus('Task was successful!');


        }catch (QueryException $exception)
        {
            return redirect()->back()->withErrors($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LocationDistrict  $locationDistrict
     * @return \Illuminate\Http\Response
     */
    public function show(LocationDistrict $locationDistrict)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LocationDistrict  $locationDistrict
     * @return \Illuminate\Http\Response
     */
    public function edit(LocationDistrict $district)
    {
        $divisionOfThisDistrict = LocationDivision::find($district->division_id);
        $divisions = LocationDivision::all();
        return view('backend.admin.district.edit',compact('district','divisionOfThisDistrict','divisions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LocationDistrict  $locationDistrict
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LocationDistrict $district)
    {
        try{
            $request->validate([
                'name'=>'required',
                'division_id'=>'required'
            ]);

            //dd($request->all());

            $district->update($request->all());

        }catch (QueryException $exception)
        {
            return redirect()->back()->withInput()->withErrors($exception->getMessage());
        }
        return redirect()->route('districts.index')->withStatus('District has been updated successfully.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LocationDistrict  $locationDistrict
     * @return \Illuminate\Http\Response
     */
    public function destroy(LocationDistrict $district)
    {
        try{
//            dd($division);

            $district->delete();

            return redirect()->route('districts.index')->withStatus('district has been deleted successful!');

        }catch (QueryException $exception){
            return redirect()->back()->withErrors($exception->getMessage());
        }
    }

}
