<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Role;

class  LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
//    protected $redirectTo = RouteServiceProvider::HOME;
    protected $redirectTo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function redirectTo()
    {
        $role_id = Auth::user()->role_id;
            $role = Role::where('id', '=', "$role_id")->first();
//            dd($role->name);

            if ($role->name === 'admin')
            {
                $this->redirectTo = route('admin.dashboard.index');
                return $this->redirectTo;
            }
            elseif ($role->name === 'manager')
            {
                //return redirect()->route('manager.dashboard.index');
                $this->redirectTo = route('manager.dashboard.index');
                return $this->redirectTo;
            }
            elseif ($role->name === 'customer')
            {
               // return redirect()->route('customer_index');
                $this->redirectTo = route('customer_index');
                return $this->redirectTo;
            }
    }
}
