<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        return view('backend.admin.role.index',compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.admin.role.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $validateData = $request->validate([
                'name'=>'required|unique:roles'
            ]);
            $validateData['name'] = strtolower($validateData['name']);
            Role::create($validateData);
            return redirect()->route('roles.index')->withStatus('Roles has been added successfully.');

        }catch (QueryException $exception)
        {
            return redirect()->back()->withInput()->withErrors($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
//        dd($role);
        return view('backend.admin.role.edit',compact('role'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        try{
            $validateData = $request->validate([
                'name'=>'required|unique:roles'
            ]);
            $validateData['name'] = strtolower($validateData['name']);
            $role->update($validateData);
            return redirect()->route('roles.index')->withStatus('Roles has been updated successfully.');

        }catch (QueryException $exception)
        {
            return redirect()->back()->withInput()->withErrors($exception->getMessage());
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {

        try{
            $role->delete();
            return redirect()->route('roles.index')->withStatus('Roles has been deleted successfully.');

        }catch (QueryException $exception)
        {
            return redirect()->back()->withInput()->withErrors($exception->getMessage());
        }
    }
}
