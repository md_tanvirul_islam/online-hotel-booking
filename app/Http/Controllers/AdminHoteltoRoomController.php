<?php

namespace App\Http\Controllers;

use App\Hotel;
use App\Room;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminHoteltoRoomController extends Controller
{
    public function index($id)
    {

        $rooms = Room::where('hotel_id','=',"$id")->get();
//        dd($rooms);
        $hotel =Hotel::where('id','=',"$id")->first();
        return view('backend.admin.hoteltoroom.index',compact('id','rooms','hotel'));
    }

    public function create($id)
    {
//        dd($id);
        $hotel =Hotel::where('id','=',"$id")->first();
        return view('backend.admin.hoteltoroom.create',compact('id','hotel'));
    }

    public function store(Request $request,$id)
    {
            try {

                $request->validate([
                    'number' => 'required',
                    'type' => 'required|',
                    'single_beds' => 'required|numeric',
                    'double_beds' => 'required|numeric',
                    'price' => 'required|numeric',
                    'TV' => 'required|',
                    'AC' => 'required|',
                    'bathtub' => 'required|',
                    'water_heater' => 'required|',
                    'refrigerator' => 'required|',
                    'wifi' => 'required|',
                    'coffee_tea_maker' => 'required|',
                    'other_facilities' => 'required|',


                ]);
                $data = $request->all();
                $data['max_person']= ((1*$data['single_beds'])+(2*$data['double_beds']));
                if ($request->hasFile('photo'))
                    {
                        $file = $request->file('photo');
                        $extension = $file->getClientOriginalExtension();
                        $filename = time().'.'.$extension;
                        $file->move('uploads/rooms/',$filename);
                        $data['photo'] = $filename;
                    }
                    else{
                        $data['photo'] = "";
                    }

                Room::create($data);
                return redirect()->route('Admin.hotel.rooms.index',[$id])->with('success','Room has been created successfully.');

            }
            catch (QueryException $exception) {
                return redirect()->back()->withInput()->with('error',$exception->getMessage());
            }
    }

    public function show($id,$room)
    {

        $room = Room::where('id','=',"$room")->first();
        $hotel = Hotel::where('id','=',"$room->hotel_id")->first();
        return view('backend.admin.hoteltoroom.show',compact('room','hotel'));
    }

    public function edit($id,$room)
    {
        $room = Room::where('id','=',"$room")->first();
        $hotel = Hotel::where('id','=',"$room->hotel_id")->first();
        return view('backend.admin.hoteltoroom.edit',compact('room','hotel','id'));
    }

    public function update(Request $request,$id,$room)
    {
        try {

            $request->validate([
                'number' => 'required',
                'type' => 'required|',
                'single_beds' => 'required|numeric',
                'double_beds' => 'required|numeric',
                'price' => 'required|numeric',
                'TV' => 'required|',
                'AC' => 'required|',
                'bathtub' => 'required|',
                'water_heater' => 'required|',
                'refrigerator' => 'required|',
                'wifi' => 'required|',
                'coffee_tea_maker' => 'required|',
                'other_facilities' => 'required|',


            ]);

            $room = Room::where('id','=',"$room")->first();
            $data = $request->all();
            if ($request->hasFile('photo'))
            {
                $file = $request->file('photo');
                $extension = $file->getClientOriginalExtension();
                $filename = time().'.'.$extension;
                $file->move('uploads/rooms/',$filename);
                $data['photo'] = $filename;
            }
            $room->update($data);
            return redirect()->route('Admin.hotel.rooms.index',[$id])->with('success','Room info has been updated successfully.');

        }
        catch (QueryException $exception) {
            return redirect()->back()->withInput()->with('error',$exception->getMessage());
        }
    }

    public function destroy($id,$room)
    {

        try{
            $room = Room::where('id','=',"$room")->first();
            $room->delete();
            return redirect()->route('Admin.hotel.rooms.index',[$id])->with('success','Room has been deleted successfully.');

        }catch (QueryException $exception)
        {
            return redirect()->back()->withInput()->with('error',$exception->getMessage());
        }
    }

    public function hotelBookings($id)
    {
        $bookings = DB::table('room_user')->where('hotel_id','=',"$id")->orderByDesc('created_at')->get();
        return view('backend.admin.hoteltoroom.booking',compact('bookings'));
    }


}
