<?php

namespace App\Http\Controllers;

use App\Hotel;
use App\Room;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $hotel = Hotel::where('user_id','=',"$user->id")->first();
        $rooms =Room::where('hotel_id','=',"$hotel->id")->get();
        return view('backend.manager.room.index',compact('rooms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $hotel = Hotel::where('user_id','=',"$user->id")->first();
        return view('backend.manager.room.create',compact('hotel'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $request->validate([
                'number' => 'required',
                'type' => 'required|',
                'single_beds' => 'required|numeric',
                'double_beds' => 'required|numeric',
                'price' => 'required|numeric',
                'TV' => 'required|',
                'AC' => 'required|',
                'bathtub' => 'required|',
                'water_heater' => 'required|',
                'refrigerator' => 'required|',
                'wifi' => 'required|',
                'coffee_tea_maker' => 'required|',
                'max_person' => 'required'
            ]);

            $data = $request->all();
            if ($request->hasFile('photo'))
            {
                $file = $request->file('photo');
                $extension = $file->getClientOriginalExtension();
                $filename = time().'.'.$extension;
                $file->move('uploads/rooms/',$filename);
                $data['photo'] = $filename;
            }
            else{
                return $request;
                $data['photo'] = "";

            }
            Room::create($data);
            return redirect()->route('rooms.index')->with('success','Room created Successfully.');


        }catch (QueryException $exception)
        {
            return redirect()->back()->with('error',$exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function show(Room $room)
    {
        $user = Auth::user();
        $hotel = Hotel::where('user_id','=',"$user->id")->first();
        return view('backend.manager.room.show',compact('room','hotel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function edit(Room $room)
    {
        $user = Auth::user();
        $hotel = Hotel::where('user_id','=',"$user->id")->first();
        return view('backend.manager.room.edit',compact('room','hotel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Room $room)
    {
        try {


            $request->validate([
                'number' => 'required',
                'type' => 'required|',
                'single_beds' => 'required|numeric',
                'double_beds' => 'required|numeric',
                'price' => 'required|numeric',
                'TV' => 'required|',
                'AC' => 'required|',
                'bathtub' => 'required|',
                'water_heater' => 'required|',
                'refrigerator' => 'required|',
                'wifi' => 'required|',
                'coffee_tea_maker' => 'required|',

            ]);

            $data = $request->all();
            if ($request->hasFile('photo'))
            {
                $file = $request->file('photo');
                $extension = $file->getClientOriginalExtension();
                $filename = time().'.'.$extension;
                $file->move('uploads/rooms/',$filename);
                $data['photo'] = $filename;
            }
            else{
                return $request;
                $data['photo'] = "";
            }

            $room->update($data);
            return redirect()->route('rooms.index')->with('success','Room info has been updated successfully.');

        }
        catch (QueryException $exception) {
            return redirect()->back()->with('error',$exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function destroy(Room $room)
    {
        try {
            $room->delete();
            return redirect()->route('rooms.index')->withStatus('Room has been deleted successfully.');

        }
        catch (QueryException $exception) {
            return redirect()->back()->with('error',$exception->getMessage());
        }
    }
}
