<?php

namespace App\Http\Controllers;

use App\Profile;
use App\Role;
use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('backend.admin.user.index',compact('users'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles =Role::all();
        return view('backend.admin.user.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $validateData = $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
                'role_id'=>['required']
            ]);

            User::create([
                'name' => $validateData['name'],
                'role_id'=>$validateData['role_id'],
                'email' => $validateData['email'],
                'password' => Hash::make($validateData['password']),
            ]);
            return redirect()->route('users.index')->withStatus('User has been created successfully.');

        }catch (QueryException $exception)
        {
            return redirect()->back()->withInput()->withErrors($exception->getMessage());
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findorFail($id);
        $profile = Profile::where('user_id','=',"$id")->first();
        return view('backend.admin.user.show',compact('user','profile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $roleOfThisUser = Role::where('id','=',"$user->role_id")->first();
//        dd($roleOfThisUser->name);
        $roles = Role::all();
        return view('backend.admin.user.edit',compact('user','roles','roleOfThisUser'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        try{
            $validateData = $request->validate([
                'name' => ['string', 'max:255'],
                'email' => ['string', 'email', 'max:255', 'unique:users,email,'.$user->id],
                'password' => ['string', 'min:8', 'confirmed'],
                'role_id'=>['exists:roles,id']

            ]);

            $user->update([
                'name' => $validateData['name'],
                'role_id'=>$validateData['role_id'],
                'email' => $validateData['email'],
                'password' => Hash::make($validateData['password']),
            ]);
            return redirect()->route('users.index')->withStatus('User info has been updated successfully.');

        }catch (QueryException $exception)
        {
            return redirect()->back()->withInput()->withErrors($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        try{
            $user->delete();
            return redirect()->route('users.index')->withStatus('Users has been deleted successfully.');

        }catch (QueryException $exception)
        {
            return redirect()->back()->withInput()->withErrors($exception->getMessage());
        }
    }

    public function admin()
    {
//        dd("hello");
        $role = Role::where('name','=','admin')->first();
        $users = User::where('role_id','=',$role->id)->get();
        return view('backend.admin.user.index',compact('users'));
    }
    public function customer()
    {
        $role = Role::where('name','=','customer')->first();
        $users = User::where('role_id','=',$role->id)->get();
        return view('backend.admin.user.index',compact('users'));
    }
    public function manager()
    {

        $role = Role::where('name','=','manager')->first();
        $users = User::where('role_id','=',$role->id)->get();
        return view('backend.admin.user.index',compact('users'));
    }
}
