<?php

namespace App\Http\Controllers;

use App\Hotel;
use App\LocationDivision;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class LocationDivisionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $divisions = LocationDivision::orderBy('created_at', 'desc')->get();
        return view('backend.admin.division.index', compact('divisions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('backend.admin.division.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {

            $request->validate([
                'name' => 'required|unique:location_divisions|max:50',

            ]);

            $data = $request->all();
            LocationDivision::create($data);
            return redirect()->route('divisions.index')->withStatus('Task was successful!');


        }catch (QueryException $exception)
        {
            return redirect()->back()->withErrors($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LocationDivision  $locationDivision
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {



    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LocationDivision  $locationDivision
     * @return \Illuminate\Http\Response
     */
    public function edit(LocationDivision $division)
    {
        return view('backend.admin.division.edit',compact('division'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LocationDivision  $locationDivision
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LocationDivision $division)
    {
        try{
            $validateData = $request->validate([
                'name'=>'required|unique:location_divisions'
            ]);

            $division->update($validateData);
            return redirect()->route('divisions.index')->withStatus('Division has been updated successfully.');

        }catch (QueryException $exception)
        {
            return redirect()->back()->withInput()->withErrors($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LocationDivision  $locationDivision
     * @return \Illuminate\Http\Response
     */
    public function destroy(LocationDivision $division)
    {
        try{
//            dd($division);

            $division->delete();

            return redirect()->route('divisions.index')->withStatus('Division has been deleted successful!');

        }catch (QueryException $exception){
            return redirect()->back()->withErrors($exception->getMessage());
        }
    }
}
