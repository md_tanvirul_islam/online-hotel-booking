<?php

namespace App\Http\Controllers;

use App\Hotel;
use App\Room;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminRoomController extends Controller
{
        public function index()
            {
                $rooms = Room::orderby('created_at','desc')->get();
                return view('backend.admin.room.index',compact('rooms'));
            }

        public function create()
            {

            }

        public function store(Request $request)
            {
            }

        public function show(Room $room_list)
            {
                $room = $room_list;
                $hotel = Hotel::where('id','=',"$room->hotel_id")->first();
                return view('backend.admin.room.show',compact('room','hotel'));
            }

        public function edit(Room $room_list)
            {
                $room = $room_list;
                $hotel = Hotel::where('id','=',"$room->hotel_id")->first();
                return view('backend.admin.room.edit',compact('room','hotel'));
            }

        public function update(Request $request, Room $room_list)
            {
                try {

                    $request->validate([
                        'number' => 'required',
                        'type' => 'required|',
                        'single_beds' => 'required|numeric',
                        'double_beds' => 'required|numeric',
                        'price' => 'required|numeric',
                        'TV' => 'required|',
                        'AC' => 'required|',
                        'bathtub' => 'required|',
                        'water_heater' => 'required|',
                        'refrigerator' => 'required|',
                        'wifi' => 'required|',
                        'coffee_tea_maker' => 'required|',
                        'other_facilities' => 'required|',

                    ]);
                    $data = $request->all();

                    if ($request->hasFile('photo'))
                    {
                        $file = $request->file('photo');
                        $extension = $file->getClientOriginalExtension();
                        $filename = time().'.'.$extension;
                        $file->move('uploads/rooms/',$filename);
                        $data['photo'] = $filename;
                    }
                    else{
                        return $request;
                        $data['photo'] = "";
                    }

                    $room_list->update($data);
                    return redirect()->route('room_lists.index')->with('success','Room info has been updated successfully.');

                    }
                catch (QueryException $exception) {
                    return redirect()->back()->with('error',$exception->getMessage());
                }
            }

        public function destroy(Room $room_list)
            {

                try{
                    $room_list->delete();
                    return redirect()->route('$room_list.index')->with('success','This Room has been deleted successfully.');

                }catch (QueryException $exception)
                {
                    return redirect()->back()->withInput()->with('error',$exception->getMessage());
                }
            }



}
