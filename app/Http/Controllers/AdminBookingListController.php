<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminBookingListController extends Controller
{
    public function index()
    {
        $bookings = DB::table('room_user')->orderBy('created_at','DESC')->get();
        return view('backend.admin.booking.index',compact('bookings'));
    }
}
