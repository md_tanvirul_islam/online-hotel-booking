<?php

namespace App\Http\Controllers;

use App\Hotel;
use App\Room;
use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Mail\CustomerBookingMail;

class CustomerBookingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function bookingPage(Request $request)
    {
        $hotel_id = $request->hotel_id;
        $room_id = $request->room_id;
        $checkin= Carbon::parse($request->checkin)->format('d M Y');
        $checkout =Carbon::parse($request->checkout)->format('d M Y');
        $noOfDays = Carbon::parse($checkin)->diffInDays($checkout);
        $hotel =Hotel::findorFail($hotel_id);
        $room = Room::findorFail($room_id);
        $amount = $room->price * $noOfDays;
        return view('frontend.elements.booking',compact('noOfDays','room','hotel','checkin','checkout','amount'));
    }

    public function bookingStore(Request $request)
    {
//        dd($request->all());
        try{
            $request->validate([

                'user_id' => 'exists:users,id',
                'room_id'=>'exists:rooms,id',
                'hotel_id'=>'exists:hotels,id'
            ]);

//            2020-09-01 year-month-day
            $checkin= Carbon::parse($request->checkin)->format('Y-m-d');
            $checkout= Carbon::parse($request->checkout)->format('Y-m-d');

//            dd($checkout);

            DB::table('room_user')->insert([
                'user_id' =>$request->user_id,
                'room_id'=>$request->room_id,
                'hotel_id' =>$request->hotel_id,
                'checkin' =>$checkin,
                'checkout' =>$checkout,
                'days'=>intval($request->days) ,
                'cost'=>intval($request->cost),
            ]);

//            Mailing to customer about the booking
//            $addedBooking = DB::table('room_user')->where('user_id','=',"$request->user_id")->where('room_id','=',"$request->room_id")->where('checkin','=',"$checkin")->where('checkout','=',"$checkout")->first();
//
//            Mail::to($request->user())->send(new CustomerBookingMail($addedBooking));
//Mailing end


            Session::forget(['hotels','division','district','checkin','checkout','today','rooms','hotel_id','room_id']);
            $request->session()->put('abc', 'Your Booking is Done.You will get a confirmation email soon.');

            return redirect()->route('customer_index')->withStatus('Your Booking is Done.You will get a confirmation email soon.');

        }catch (QueryException $exception)
        {
            dd($exception);
            return redirect()->back()->withInput()->withErrors($exception->getMessage());
        }
    }
}
