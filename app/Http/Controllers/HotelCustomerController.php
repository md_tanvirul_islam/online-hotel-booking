<?php

namespace App\Http\Controllers;

use App\Hotel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HotelCustomerController extends Controller
{
   public function index()
   {
       $managerId = Auth()->id();
       $hotel = Hotel::where('user_id','=',"$managerId")->first();
       $bookings = DB::table('room_user')->where('hotel_id','=',"$hotel->id")->get();
       return view('backend.manager.user.index',compact('bookings'));

   }
}
