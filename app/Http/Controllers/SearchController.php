<?php

namespace App\Http\Controllers;

use App\Hotel;
use App\LocationDistrict;
use App\LocationDivision;
use App\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Session;

class SearchController extends Controller
{
    public function search(Request $request)
    {
//        dd($request->all());
        if (Session::has(['hotels','division','district','checkin','checkout']))
        {
            $hotels = session('hotels');
            $division = session('division');
            $district = session('district');
            $checkin = session('checkin');
            $checkout = session('checkout');
            $today = session('today');

        }
        else
        {
            $checkin = Carbon::parse($request->checkin)->toDateString();
            $checkout =Carbon::parse($request->checkout)->toDateString();
            $today = Carbon::today()->toDateString();
            $division = LocationDivision::where('id','=',"$request->division_id")->select('id','name')->first();
            $district = LocationDistrict::where('id','=',"$request->district_id")->select('id','name','division_id')->first();
            $hotels = Hotel::where('division_id','=',"$division->id")->get();
            session([
                'hotels'=>$hotels,
                'division'=> $division,
                'district'=>$district,
                'checkin'=>$checkin,
                'checkout'=>$checkout,
                'today'=>$today,
            ]);
        }

        foreach ($hotels as $eachHotel)
        {
            $hotel_rooms = Room::where('hotel_id','=',"$eachHotel->id")->get();


            foreach ($hotel_rooms as $eachRoom)
            {
                $checking = DB::table('room_user')->where('room_id','=',"$eachRoom->id")->exists();

                if($checking == false)
                {
                    $rooms[]= $eachRoom;
                }

                if($checking == true)
                {
                 $bookings = DB::table('room_user')->where('room_id','=',"$eachRoom->id")->where('checkin','>=',"$today")->get();
                   foreach ($bookings as $booking)
                   {
                       if(($checkin < $booking->checkin && $checkout< $booking->checkin) || ($checkin > $booking->checkout && $checkout > $booking->checkout))
                       {
                           $rooms[]= $eachRoom;
                       }
                   }

                }

            }
        }

        Session::put('rooms',$rooms);
//        dd(Session::all());


        return view('frontend.elements.search_result');


    }

    public function show(Request $request)
    {
        if (Session::has(['hotel_id','room_id','division','district','checkin','checkout']))
        {
            $hotel_id = session('hotel_id');
            $room_id = session('room_id');
            $checkin = session('checkin');
            $checkout = session('checkout');

        }
        else
        {
            session([
                'hotel_id'=>$request->hotel_id,
                'room_id'=>$request->room_id,
            ]);
            $room_id = $request->room_id;
            $hotel_id = $request->hotel_id;
            $checkin = $request->checkin;
            $checkout =  $request->checkout;
        }

        $room = Room::findorFail($room_id);
        $hotel = Hotel::findorFail($hotel_id);
        $division= LocationDivision::findorFail($hotel->division_id);
        $district = LocationDistrict::findorFail($hotel->district_id);
        return view('frontend.elements.search_show',compact('room','hotel','division','district','checkin','checkout'));
    }
}
