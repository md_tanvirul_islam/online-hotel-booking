<?php

namespace App\Http\Controllers;

use App\Hotel;
use App\LocationDistrict;
use App\LocationDivision;
use App\Role;
use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class AdminHotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hotels =Hotel::all();
        return view('backend.admin.hotel.index',compact('hotels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $divisions = LocationDivision::all();
        $districts = LocationDistrict::all();
        $users = User::all();
        return view('backend.admin.hotel.create',compact("divisions","districts"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $request->validate([
                'name' => 'required',
                'division_id' => 'required|',
                'district_id' => 'required',
                'local_address' => 'required|',
                'telephone' => 'required|numeric',
                'mobile1' => 'required|numeric',
                'mobile2' => 'required|numeric',
                'star' => 'required|',
                'email'=>'exists:users,email'

            ]);

            $manager = User::where('email','=',"$request->email")->first();
            $data = $request->all();

            if(Hotel::where('user_id','=',"$manager->id")->first())
            {
                return redirect()->back()->withErrors('This manager owns a hotel. Try with another email');
            }
            else
            {
                $data['user_id']=$manager->id;
                if ($request->hasFile('photo'))
                {
                    $file = $request->file('photo');
                    $extension = $file->getClientOriginalExtension();
                    $filename = time().'.'.$extension;
                    $file->move('uploads/hotels/',$filename);
                    $data['photo'] = $filename;
                }

                Hotel::create($data);
                return redirect()->route('hotel_lists.index')->with('success','The Hotel has been created successful!');

            }

        }catch (QueryException $exception)
        {
            return redirect()->back()->withErrors($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Hotel $hotel_list)
    {

        $hotel = $hotel_list;
        $manager=User::where('id','=',"$hotel->user_id")->first();
        $division = LocationDivision::findorFail($hotel->division_id);
        $district = LocationDistrict::findorFail($hotel->district_id);
        return view('backend.admin.hotel.show',compact('hotel','division','district','manager'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Hotel $hotel_list)
    {
        $hotel = $hotel_list;
        $manager=User::where('id','=',"$hotel->user_id")->first();
        $thisDivision = LocationDivision::findorFail($hotel->division_id);
        $thisDistrict = LocationDistrict::findorFail($hotel->district_id);
        $divisions = LocationDivision::all();
        $districts = LocationDistrict::all();
        return view('backend.admin.hotel.edit',compact('hotel','thisDistrict','thisDivision','divisions','districts','manager'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hotel $hotel_list)
    {
        try {

            $request->validate([
                'name' => 'required',
                'division_id' => 'required|',
                'district_id' => 'required',
                'local_address' => 'required|',
                'telephone' => 'required|numeric',
                'mobile1' => 'required|numeric',
                'mobile2' => 'required|numeric',
                'star' => 'required|',
                'email'=>'exists:users,email'

            ]);

            $manager = User::where('email','=',"$request->email")->first();
            $hotel = Hotel::where('user_id','=',"$manager->id")->first();
            $role = Role::where('id','=',"$manager->role_id")->first();

            $data = $request->all();

            if ($request->hasFile('photo'))
            {
                $file = $request->file('photo');
                $extension = $file->getClientOriginalExtension();
                $filename = time().'.'.$extension;
                $file->move('uploads/hotels/',$filename);
                $data['photo'] = $filename;
            }
            if($manager && $hotel== null)
            {
                if($role->name=="manager")
                {
                    $data['user_id']=$manager->id;
                    $hotel_list->update($data);
                    return redirect()->route('hotel_lists.index')->with('success','The Hotel has been updated successful!');
                }
                else
                {
                    return redirect()->back()->withInput()->with('error','You can not assign an user as hotel manager who does not have manager role');
                }
            }
            elseif($manager && $hotel->id==$request->id)
            {
                if($role->name=="manager")
                {
                    $data['user_id']=$manager->id;
                    $hotel_list->update($data);
                    return redirect()->route('hotel_lists.index')->with('success','The Hotel has been updated successful!');
                }
                else
                {
                    return redirect()->back()->withInput()->withErrors('You can not assign an user as hotel manager who does not have manager role');
                }
            }
            elseif($manager && $hotel->id!=$request->id)
            {
                return redirect()->back()->withErrors('This manager owns a hotel. Try with another Manager email. First create a manager account,if You dont have another email');
            }


        }catch (QueryException $exception)
        {
            return redirect()->back()->withErrors($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
