<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Room extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'hotel_id','number','type','single_beds','double_beds','max_person','price','TV','AC','bathtub','water_heater','refrigerator','wifi','coffee_tea_maker','other_facilities','photo'

         ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [

    ];

    public function hotel()
    {
        return $this->belongsTo(Hotel::class);
    }

    public function users()
    {
        return $this->belongsToMany( User::class);
    }


}
