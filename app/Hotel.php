<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Hotel extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','user_id','division_id','district_id','local_address','telephone','mobile1','mobile2','star','free_breakfast','about_breakfast','private_parking','car_rental','swimming_pool','call_on_doctor','gym','restaurant','spa','meeting_room','other_facilities','photo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [

    ];

    public function user()
    {
//        return $this->belongsTo(User::class);
        return $this->belongsTo('App\User');

    }
    public function division()
    {
        return $this->belongsTo(LocationDivision::class);
    }
    public function district()
    {
        return $this->belongsTo(LocationDistrict::class);
    }
    public function room()
    {
        return $this->hasMany(Room::class);
    }


}
